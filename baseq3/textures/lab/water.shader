shader_type spatial;

uniform sampler2D noise;
uniform sampler2D tex;
uniform float time_mul;
uniform float uv_mul;
uniform float offset_mul;
uniform float result_alpha;

uniform float cut_depth_a;
uniform float cut_depth_b;
uniform float debug_at_max;

void fragment() {
	vec2 uv = (time_mul * TIME) + (UV * uv_mul);
	vec2 uvB = ((time_mul * TIME) / 2.0) + (UV * uv_mul);
	vec2 offset = (texture(noise, uv).xy - 0.5);
	offset *= offset_mul;
	ALBEDO = texture(tex, uvB + (offset * 8.0)).rgb;
	// such a cheat!
	float depth_conv = abs(UV.y);
	float depth_fade = 1.0 - clamp(((depth_conv - cut_depth_a) / (cut_depth_b - cut_depth_a)), 0.0, 1.0);
	depth_fade *= depth_fade * depth_fade;
	depth_fade = depth_fade >= 1.0 ? debug_at_max : depth_fade;
	ALPHA = result_alpha * depth_fade;
}
