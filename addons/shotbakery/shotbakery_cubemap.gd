tool
extends Position3D
class_name ShotBakeryCubemap

export var viewport_preset: Resource = preload("default_viewport_preset.tres")

export(int, LAYERS_3D_RENDER) var cull_mask: int = ShotBakeryCamera.SAFE_CULL_MASK

export var frustrum_size: float = 2.0 setget _set_frustrum_size

# For an accurate cubemap, set to half of Frustrum Size.
export var z_near: float = 1.0 setget _set_z_near
export var z_far: float = 5000.0 setget _set_z_far
export var apply_transform_rotation: bool = false setget _set_apply_transform_rotation

# Note that if a primary cubemap is not present,
#  a primary cubemap will be generated but not stored here.
# It will be stored in the cubemap setup record (if any) if requested.
# The same mostly occurs with the secondary target.
export var primary_setup_record: bool = true
export var primary_image_size: int = 128
export var primary_target: CubeMap

# The secondary cubemap is intended for coarse reflections,
#  or potentially as part of a cubemap-based general lighting system.
# The secondary cubemap is therefore downscaled from the primary.
# It's important to note how the cubemap size of 1 makes the diffuse work well
#  without needing any actual blurring.
# NOTE: THIS MAY BE BLURRED IN FUTURE.
export var secondary_setup_record: bool = true
export var secondary_downscale_size: int = 1
export var secondary_target: CubeMap

# written into ShotBakerySetupCubeMapRecord if you're using it
export var setup_record_notes: String = ""

var tmp_viewports
var tmp_cameras

# Setters

func _set_frustrum_size(v):
	frustrum_size = v
	update_gizmo()

func _set_z_near(v):
	z_near = v
	update_gizmo()

func _set_z_far(v):
	z_far = v
	update_gizmo()

func _set_apply_transform_rotation(v):
	apply_transform_rotation = v
	update_gizmo()

# The Rest

func get_effective_global_transform():
	var effective_global_transform = global_transform
	if not apply_transform_rotation:
		effective_global_transform = Transform.IDENTITY.translated(global_transform.origin)
	return effective_global_transform

func shotbakery_bake_handler(mode: int, setup: ShotBakerySetup):
	if primary_image_size <= 0:
		push_warning("Attempted to bake a cubemap, " + str(get_path()) + ", with a bad size.")
		return
	var svp: ShotBakeryViewportPreset = viewport_preset
	if svp == null:
		push_warning("Attempted to bake a cubemap, " + str(get_path()) + ", with no ShotBakeryViewportPreset.")
		return
	if not svp.validate():
		push_warning("Attempted to bake a cubemap, " + str(get_path()) + ", with a bad ShotBakeryViewportPreset.")
		return
	if mode == ShotBakerySetup.BAKE_HANDLER_MODE_SETUP:
		var transforms = []
		# L/R
		transforms.push_back(Transform.IDENTITY.rotated(Vector3(0, 1, 0), PI / 2))
		transforms.push_back(Transform.IDENTITY.rotated(Vector3(0, -1, 0), PI / 2))
		# B/T
		transforms.push_back(Transform.IDENTITY.rotated(Vector3(-1, 0, 0), PI / 2))
		transforms.push_back(Transform.IDENTITY.rotated(Vector3(1, 0, 0), PI /2))
		# F/B
		transforms.push_back(Transform.IDENTITY)
		transforms.push_back(Transform.IDENTITY.rotated(Vector3(0, 1, 0), PI))
		var effective_global_transform = get_effective_global_transform()
		for i in range(6):
			transforms[i] = effective_global_transform * transforms[i]
		tmp_viewports = []
		tmp_cameras = []
		for i in range(6):
			var tmp_viewport = VisualServer.viewport_create()
			var tmp_camera = VisualServer.camera_create()
			tmp_viewports.push_back(tmp_viewport)
			tmp_cameras.push_back(tmp_camera)

			VisualServer.camera_set_cull_mask(tmp_camera, cull_mask)
			VisualServer.camera_set_frustum(tmp_camera, frustrum_size, Vector2(), z_near, z_far)
			VisualServer.camera_set_transform(tmp_camera, transforms[i])

			svp.apply_to_viewport(self, tmp_viewport, tmp_camera, primary_image_size, primary_image_size)
			VisualServer.viewport_set_vflip(tmp_viewport, true)
	elif mode == ShotBakerySetup.BAKE_HANDLER_MODE_FINISH:
		var efpc = primary_target
		if efpc == null:
			efpc = CubeMap.new()
		var efsc = secondary_target
		if efsc == null and secondary_setup_record:
			efsc = CubeMap.new()
		for i in range(6):
			var tmp_viewport = tmp_viewports[i]
			var tmp_camera = tmp_cameras[i]
			var tex = VisualServer.viewport_get_texture(tmp_viewport)
			VisualServer.force_sync()
			var img = VisualServer.texture_get_data(tex)
			if i >= 4 or i <= 1:
				img.flip_x()
			if i == 2 or i == 3:
				img.flip_y()
			VisualServer.force_sync()
			img = svp.process_img(img)
			efpc.set_side(i, img)
			if efsc != null:
				var dup: Image = img.duplicate()
				dup.resize(secondary_downscale_size, secondary_downscale_size, Image.INTERPOLATE_LANCZOS)
				efsc.set_side(i, dup)
			VisualServer.force_sync()
		for i in range(6):
			var tmp_viewport = tmp_viewports[i]
			var tmp_camera = tmp_cameras[i]
			VisualServer.free_rid(tmp_viewport)
			VisualServer.free_rid(tmp_camera)
		# these two, but particularly the last,
		#  are why the UI updates properly on bake
		property_list_changed_notify()
		if setup != null:
			if setup.setup_cubemap_record != null:
				if not primary_setup_record:
					efpc = null
				if not secondary_setup_record:
					efsc = null
				if efpc != null or efsc != null:
					setup.setup_cubemap_record.add_cubemap(global_transform.origin, efpc, efsc, name, setup_record_notes)
