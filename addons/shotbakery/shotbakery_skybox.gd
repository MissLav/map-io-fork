class_name ShotBakerySkybox
extends MeshInstance
tool

export var skybox: CubeMap setget set_skybox

func set_skybox(sb: CubeMap):
	skybox = sb
	material_override.set_shader_param("cube", skybox)

func _init():
	mesh = preload("shaders/skybox_cube.tres")
	material_override = ShaderMaterial.new()
	material_override.shader = preload("shaders/skybox.gdshader")
	cast_shadow = SHADOW_CASTING_SETTING_OFF
	use_in_baked_light = false
	generate_lightmap = false
