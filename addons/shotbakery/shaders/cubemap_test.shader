shader_type spatial;
render_mode depth_draw_opaque,cull_back,unshaded,world_vertex_coords;

uniform samplerCube cube;

varying vec3 normal;

void vertex() {
	normal = NORMAL;
}

void fragment() {
	ALBEDO = texture(cube, normal).rgb;
}
