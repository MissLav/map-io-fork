tool
extends EditorPlugin

var cubemap_gizmo: ShotBakeryCubemapGizmoPlugin = ShotBakeryCubemapGizmoPlugin.new()
var is_visible: bool
var dock: Control

func _enter_tree():
	dock = preload("dock.tscn").instance()
	dock.set_meta("editor_plugin", self)
	cubemap_gizmo.set_meta("editor_plugin", self)
	add_spatial_gizmo_plugin(cubemap_gizmo)

func handles(object):
	return object.has_method("shotbakery_bake_handler")

func make_visible(visible):
	if visible and not is_visible:
		add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, dock)
	if is_visible and not visible:
		remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, dock)
	is_visible = visible

func _exit_tree():
	make_visible(false)
	remove_spatial_gizmo_plugin(cubemap_gizmo)
	dock.free()
