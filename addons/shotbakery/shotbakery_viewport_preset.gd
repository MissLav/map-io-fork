tool
extends Resource
class_name ShotBakeryViewportPreset

# Not sure how well setting this actually works.
export var hdr: bool = false
# If the system you're baking the cubemap on supports it,
#  MSAA can increase cubemap quality without supersampling.
# However, it's *lower quality*.
export var msaa_setting: int = 0
# FXAA is as compatible as MSAA, but is ultimately a postprocessing filter.
export var fxaa_setting: bool = false
# A slower but more compatible and higher quality anti-aliasing method.
# To summarize, higher quality images are rendered and downscaled.
# Particularly good for old Intel integrated graphics (Ironlake), which cannot use MSAA.
export(int, 1, 16) var supersampling_scale: int = 1
# Supersampling interpolation.
# You usually have no reason to mess with this.
export var supersampling_interp: int = Image.INTERPOLATE_LANCZOS
# Not even sure if it works.
export var draw_mode: int = 0
# Transparency. Note you're apparently not OK to use this for cubemaps...
export var transparent_background: bool = false

func apply_to_viewport(me: Node, v: RID, c: RID, w: int, h: int):
	w *= supersampling_scale
	h *= supersampling_scale
	if w >= 16384 or h >= 16384:
		push_warning("A viewport preset went beyond sane image size ranges.")
		w = supersampling_scale
		h = supersampling_scale
	VisualServer.viewport_set_size(v, w, h)
	VisualServer.viewport_set_scenario(v, me.get_viewport().find_world().scenario)
	VisualServer.viewport_attach_camera(v, c)
	VisualServer.viewport_set_disable_3d(v, false)
	VisualServer.viewport_set_disable_environment(v, false)
	VisualServer.viewport_set_hdr(v, hdr)
	VisualServer.viewport_set_msaa(v, msaa_setting)
	VisualServer.viewport_set_use_fxaa(v, fxaa_setting)
	VisualServer.viewport_set_clear_mode(v, VisualServer.VIEWPORT_CLEAR_ALWAYS)
	VisualServer.viewport_set_update_mode(v, VisualServer.VIEWPORT_UPDATE_ONCE)
	VisualServer.viewport_set_debug_draw(v, draw_mode)
	VisualServer.viewport_set_transparent_background(v, transparent_background)
	VisualServer.viewport_set_active(v, true)

func validate():
	if supersampling_scale <= 0:
		push_warning("A viewport preset had a <= 0 supersampling scale, see details below.")
		return false
	return true

func process_img(img: Image) -> Image:
	# Finally, resize the image.
	if supersampling_scale != 1:
		var w = int(img.get_width() / supersampling_scale)
		var h = int(img.get_height() / supersampling_scale)
		img.resize(w, h, supersampling_interp)
	return img
