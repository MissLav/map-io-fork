tool
extends Button

func _pressed():
	var plugin: EditorPlugin = get_meta("editor_plugin")
	var inf = plugin.get_editor_interface()
	var sel = inf.get_selection()
	var nodes = sel.get_selected_nodes()
	if len(nodes) != 1:
		push_warning(str(len(nodes)) + " nodes were selected!")
		return
	var node = nodes[0]
	if node.has_method("shotbakery_bake_handler"):
		print("ShotBakery: Beginning")
		node.shotbakery_bake_handler(ShotBakerySetup.BAKE_HANDLER_MODE_SETUP, null)
		VisualServer.force_sync()
		VisualServer.force_draw()
		VisualServer.force_sync()
		node.shotbakery_bake_handler(ShotBakerySetup.BAKE_HANDLER_MODE_FINISH, null)
		print("ShotBakery: Done")
	else:
		push_warning("Node " + str(node.get_path()) + " was not bakable (how)")
