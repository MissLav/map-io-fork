tool
extends Camera
class_name ShotBakeryCamera

# A critical detail that ought to be explicitly described is that
#  the deliberate indirection into using a filename is because otherwise it gets
#  hard to use the resulting image as part of a pipeline.
export var baked_image_file_name: String = "output.png"
export var baked_image_exr: bool = false
export var baked_image_width: int = 1920
export var baked_image_height: int = 1080
export var viewport_preset: Resource = preload("default_viewport_preset.tres")

const SAFE_CULL_MASK = 0xFFFFF

var tmp_viewport: RID

func _init():
	cull_mask = SAFE_CULL_MASK

func shotbakery_bake_handler(mode: int, setup: ShotBakerySetup):
	if (baked_image_width <= 0) or (baked_image_height <= 0):
		push_warning("Attempted to bake a camera, " + str(get_path()) + ", with a bad size.")
		return
	var svp: ShotBakeryViewportPreset = viewport_preset
	if svp == null:
		push_warning("Attempted to bake a camera, " + str(get_path()) + ", with no ShotBakeryViewportPreset.")
		return
	if not svp.validate():
		push_warning("Attempted to bake a camera, " + str(get_path()) + ", with a bad ShotBakeryViewportPreset.")
		return
	if mode == ShotBakerySetup.BAKE_HANDLER_MODE_SETUP:
		tmp_viewport = VisualServer.viewport_create()
		svp.apply_to_viewport(self, tmp_viewport, get_camera_rid(), baked_image_width, baked_image_height)
		VisualServer.viewport_set_vflip(tmp_viewport, true)
	elif mode == ShotBakerySetup.BAKE_HANDLER_MODE_FINISH:
		var tex = VisualServer.viewport_get_texture(tmp_viewport)
		VisualServer.force_sync()
		var img = VisualServer.texture_get_data(tex)
		VisualServer.force_sync()
		img = svp.process_img(img)
		if baked_image_file_name != "":
			if baked_image_exr:
				img.save_exr(baked_image_file_name)
			else:
				img.save_png(baked_image_file_name)
		print("Saved: " + baked_image_file_name)
		VisualServer.force_sync()
		VisualServer.free_rid(tmp_viewport)
