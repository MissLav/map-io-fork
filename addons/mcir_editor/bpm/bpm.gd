extends Label

var times: Array

func _init():
	times = []

func _on_Piano_piano_key(k, p):
	if not p:
		return
	times.append(OS.get_ticks_usec())
	_recalc()

func _on_Button_pressed():
	times.clear()
	_recalc()

func _recalc():
	if times.size() < 2:
		text = "Not enough information."
	else:
		var tx = ""
		var spb = 0.0
		var i = 1
		while i < len(times):
			var usec = times[i] - times[i - 1]
			spb += float(usec) / 1000000.0
			i += 1
		spb /= i - 1
		tx += "Average seconds per beat: " + str(spb) + "\n"
		tx += "Therefore BPM: " + str(60.0 / spb) + "\n"
		text = tx
