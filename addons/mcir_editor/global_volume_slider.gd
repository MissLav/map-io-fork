extends HSlider

func _on_spacing_value_changed(value):
	var l = value / 100.0
	AudioServer.set_bus_volume_db(0, linear2db(l))
