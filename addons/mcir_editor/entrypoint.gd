class_name MCIREditorEntrypoint
extends Control

var circuit: MCIRDataCircuit
signal circuit_changed()
# context...
var pattern: String = ""
var piece: String = ""
signal context_changed()

onready var ctxlabel: Label = find_node("mcir_entrypoint_context")

static func of(n: Node) -> Node:
	while n != null:
		if n.has_method("_very_secret_mcireditorentrypoint_method"):
			return n
		n = n.get_parent()
	push_error("call to MCIREditorEntrypoint.of did not work out")
	return null

# detection
func _very_secret_mcireditorentrypoint_method():
	pass

func msgbox(s: String):
	print(s)

func ecc():
	emit_signal("circuit_changed")

func exc():
	ctxlabel.text = "Pattern: " + pattern + " | Piece: " + piece
	emit_signal("context_changed")

func ecxc():
	ecc()
	exc()

func get_current_pattern() -> MCIRDataPatternBase:
	if circuit.patterns.has(pattern):
		return circuit.patterns[pattern]
	return null

func get_current_piece() -> MCIRDataPiece:
	if circuit.pieces.has(piece):
		return circuit.pieces[piece]
	return null

func _init():
	circuit = MCIRDataCircuit.new()

func fixup_file_dialog(fd: FileDialog, o: Object, s: String):
	$body.add_child(fd)
	fd.show_modal(true)
	fd.resizable = true
	fd.rect_position += Vector2(100, 100)
	fd.rect_size += Vector2(100, 200)
	fd.invalidate()
	fd.connect("confirmed", self, "_fixup_fd_cb", [fd, o, s])

func _fixup_fd_cb(fd: FileDialog, o: Object, s: String):
	o.call(s, fd.current_path)
	fd.queue_free()

func _on_load_pressed():
	var fd = FileDialog.new()
	fd.mode = FileDialog.MODE_OPEN_FILE
	fixup_file_dialog(fd, self, "_load_file")

func _load_file(s: String):
	# deselect stuff
	pattern = ""
	piece = ""
	exc()
	# begin load
	var r = load(s)
	if r is MCIRDataCircuit:
		circuit = r
		ecc()
	else:
		msgbox("that is not a circuit")

func _on_save_pressed():
	if circuit.resource_path == "":
		var fd = FileDialog.new()
		fd.mode = FileDialog.MODE_SAVE_FILE
		fixup_file_dialog(fd, self, "_save_file")
	else:
		ResourceSaver.save(circuit.resource_path, circuit, 0)

func _save_file(s: String):
	print("saving " + s)
	ResourceSaver.save(s, circuit, ResourceSaver.FLAG_CHANGE_PATH)

func _ready():
	ecxc()
