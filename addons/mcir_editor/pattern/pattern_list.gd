extends Node

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)
onready var ple: MCIREditorPatternListEmbedded = $ple
onready var le: LineEdit = $hbc2/LineEdit

func _ready():
	ep.connect("context_changed", self, "_context_changed")
	# editor being a pain. no matter.
	ple.connect("pattern_selected", self, "_pattern_selected")

func _context_changed():
	le.text = ep.pattern

func check_pattern_name(dst: String):
	if dst == "":
		ep.msgbox("pattern name empty")
		return true
	if ep.circuit.patterns.has(dst):
		ep.msgbox("pattern already with that name")
		return true
	return false

func _on_new_audptn_pressed():
	var c = ep.circuit
	if check_pattern_name(le.text):
		return
	c.patterns[le.text] = MCIRDataPatternAudio.new()
	ep.pattern = le.text
	ep.ecc()

func _on_new_reelptn_pressed():
	var c = ep.circuit
	if check_pattern_name(le.text):
		return
	c.patterns[le.text] = MCIRDataPatternReel.new()
	ep.pattern = le.text
	ep.ecc()

func _on_new_dupptn_pressed():
	var c = ep.circuit
	if check_pattern_name(le.text):
		return
	if not c.patterns.has(ep.pattern):
		ep.msgbox("no workable pattern selected!")
		return
	var cc: MCIRDataPatternBase = c.patterns[ep.pattern]
	c.patterns[le.text] = cc.dup_pattern()
	ep.pattern = le.text
	ep.ecc()

func _on_rename_pattern_pressed():
	var dst = le.text
	var c = ep.circuit
	if check_pattern_name(dst):
		return
	var src = ep.pattern
	if not c.patterns.has(src):
		ep.msgbox("no workable pattern selected!")
		return
	c.patterns[dst] = c.patterns[src]
	c.rename_pattern_refs(src, dst)
	c.patterns.erase(src)
	ep.pattern = dst
	ep.exc()
	ep.ecc()

func _pattern_selected(text):
	# actually set to selected for convenience reasons
	ep.pattern = text
	ep.exc()
