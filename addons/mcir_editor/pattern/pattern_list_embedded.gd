class_name MCIREditorPatternListEmbedded
extends Tree

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)
onready var pe: MCIREditorPlaybackExecutive = MCIREditorPlaybackExecutive.of(self)

signal pattern_selected(pattern) # (pattern: String)

# various internal stuff
var _current_root: TreeItem
var _all_items: Dictionary

func _ready():
	ep.connect("circuit_changed", self, "_circuit_changed")
	_circuit_changed()

func _circuit_changed():
	clear()
	_current_root = create_item()
	_all_items = {}
	var c = ep.circuit
	var keys = c.patterns.keys()
	keys.sort()
	for v in keys:
		_ensure_item(v)

func _ensure_item(v: String) -> TreeItem:
	if _all_items.has(v):
		return _all_items[v]
	# The reason 'realness' is determined this way,
	#  is because it may be nice organizationally to have a root pattern,
	#  and then modified copies underneath it.
	# This matches my personal construction technique.
	# In this case we really DON'T want to misidentify.
	var real = null
	if ep.circuit.patterns.has(v):
		real = ep.circuit.patterns[v]
	var ll = v.find_last("/")
	var other = false
	var par: TreeItem = _current_root
	var name: String = v
	if ll != -1:
		# there is a predecessor
		par = _ensure_item(v.substr(0, ll))
		name = v.substr(ll + 1)
	# in any case...
	var ci = create_item(par)
	ci.set_text(0, name)
	if real != null:
		ci.add_button(0, preload("res://addons/mcir_editor/icons/play.png"))
		if real is MCIRDataPatternAudio:
			ci.set_icon(0, preload("res://addons/mcir_editor/icons/audio.png"))
		else:
			ci.set_icon(0, preload("res://addons/mcir_editor/icons/pattern.png"))
		ci.set_meta("value", v)
	_all_items[v] = ci
	return ci

func _on_ple_item_selected():
	var se = get_selected()
	if se == null:
		return
	if not se.has_meta("value"):
		return
	emit_signal("pattern_selected", se.get_meta("value"))

func _on_ple_button_pressed(item: TreeItem, column: int, id: int):
	if not item.has_meta("value"):
		return
	$piano.grab_focus()
	$piano_play_manager.current_pattern = item.get_meta("value")

func select_item(s: String):
	if _all_items.has(s):
		_all_items[s].select(0)
		scroll_to_item(_all_items[s])
