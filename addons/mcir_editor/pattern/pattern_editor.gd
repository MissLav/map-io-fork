extends Control

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)
onready var pe: MCIREditorPlaybackExecutive = MCIREditorPlaybackExecutive.of(self)
onready var ppm: MCIREditorPianoPlayManager = $piano_play_manager

var current_pattern_editor: Node
var current_pattern_editor_target: MCIRDataPatternBase = null

func _ready():
	ep.connect("context_changed", self, "_context_changed")

func _context_changed():
	var pb = ep.get_current_pattern()
	ppm.current_pattern = ep.pattern
	if pb == current_pattern_editor_target:
		return
	current_pattern_editor_target = pb
	# -- confirmed changeover no matter what (point of no return) --
	if current_pattern_editor != null:
		current_pattern_editor.queue_free()
		current_pattern_editor.get_parent().remove_child(current_pattern_editor)
		current_pattern_editor = null
	# --
	# stop if pattern is null
	if pb == null:
		return
	var psc: PackedScene = load("res://addons/mcir_editor/pattern/" + pb.mcir_pattern_id() + ".tscn")
	var pi: Control = psc.instance()
	pi.mcir_pated_setup(pb)
	pi.size_flags_vertical = SIZE_EXPAND_FILL
	current_pattern_editor = pi
	$VBoxContainer.add_child(pi)

func _on_play_pressed():
	if current_pattern_editor_target != null:
		pe.play_pattern_obj(current_pattern_editor_target, 0.0, 0.0)
