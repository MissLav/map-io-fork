class_name MCIREditorReelPatEd
extends MCIREditorPatEdBase

var reel: MCIRDataPatternReel
var note: MCIRDataPatternReelNote
var cached_default_pattern: String

signal note_changed()

func set_note(n: MCIRDataPatternReelNote):
	if is_instance_valid(note):
		note.disconnect("changed", self, "_refire_changed")
	note = n
	emit_signal("note_changed")
	if is_instance_valid(note):
		note.connect("changed", self, "_refire_changed")

func _refire_changed():
	emit_signal("note_changed")

func mcir_pated_setup(r: MCIRDataPatternReel):
	reel = r

static func of(n: Node) -> Node:
	while n != null:
		if n.has_method("_very_secret_mcireditorreelpated_method"):
			return n
		n = n.get_parent()
	push_error("call to MCIREditorReelPatEd.of did not work out")
	return null

func _very_secret_mcireditorreelpated_method():
	pass

# stuff for float boilers
func mcirreelpe_gr():
	return reel
func mcirreelpe_pr():
	reel.emit_changed()

func mcirreelpe_gn():
	return note
func mcirreelpe_pn():
	note.emit_changed()
	reel.poke_note(note)
