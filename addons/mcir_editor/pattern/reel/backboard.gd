extends Control

onready var re: MCIREditorReelPatEd = MCIREditorReelPatEd.of(self)
onready var pe: MCIREditorPlaybackExecutive = MCIREditorPlaybackExecutive.of(self)
onready var vps: MCIREditorPatternReelVPS = get_parent()

var estimator_ptr: MCIRExecPatternBase = null

func _ready():
	pe.connect("started_pattern_playback", self, "_start_estimator")

func _draw():
	var cs = MCIREditorPatternReelVPS.C_S
	for i in range((rect_size.x / cs.x) + 1):
		_draw_vline(i, Color.black)
	for j in range((rect_size.y / cs.y) + 1):
		var c = Color.black
		# highlight base note
		if j == MCIREditorPatternReelVPS.BASENOTE or j == (MCIREditorPatternReelVPS.BASENOTE + 1):
			c = Color.red
		draw_line(Vector2(0, j * cs.y), Vector2(rect_size.x, j * cs.y), c)
	_draw_vline(re.reel.sustain_start, Color.red)
	_draw_vline(re.reel.sustain_end, Color.red)
	if is_instance_valid(estimator_ptr):
		var ebt = MCIRMaths.time_beat(re.reel.bpm, estimator_ptr.mcir_exec_pattern_time())
		_draw_vline(ebt, Color.white)

func _draw_vline(i: float, c: Color):
	var cs = MCIREditorPatternReelVPS.C_S
	draw_line(Vector2((i * cs.x) + 1, 0), Vector2((i * cs.x) + 1, rect_size.y), c)

func _start_estimator(pattern: MCIRDataPatternBase, seektime: float, note: float, px: MCIRExecPatternBase):
	light(MCIREditorPatternReelVPS.map_note_br_fwd(note))
	if pattern != re.reel:
		return
	estimator_ptr = px
	set_process(true)

func _process(delta):
	update()
	if not is_instance_valid(estimator_ptr):
		set_process(false)

func _gui_input(event):
	if event is InputEventMouseButton:
		var mb: InputEventMouseButton = event
		if mb.button_index == BUTTON_RIGHT:
			if mb.pressed:
				vps.create_note_at_baseboard(mb.position)

func light(at: float):
	var cs = MCIREditorPatternReelVPS.C_S
	var pi: Control = preload("piano_line.tscn").instance()
	pi.rect_size = Vector2(rect_size.x, cs.y)
	pi.rect_position = Vector2(0.0, cs.y * at)
	add_child(pi)
