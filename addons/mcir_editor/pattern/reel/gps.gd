extends Node

export var gpsprop = ""

onready var re: MCIREditorReelPatEd = MCIREditorReelPatEd.of(self)
onready var le: MCIREditorLinegraph = MCIREditorLinegraph.of(self)

func _ready():
	re.connect("note_changed", self, "_note_changed")

func _note_changed():
	if re.note != null:
		le.set_target(re.note.get(gpsprop))
	else:
		le.set_target(null)
