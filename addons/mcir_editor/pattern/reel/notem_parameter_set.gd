extends Control

onready var re: MCIREditorReelPatEd = MCIREditorReelPatEd.of(self)
onready var pe: MCIREditorPlaybackExecutive = MCIREditorPlaybackExecutive.of(self)
onready var dg: CheckBox = $dg

func _on_add_pressed():
	var note = MCIRDataPatternReelNote.new()
	note.pattern = re.cached_default_pattern
	re.reel.add_note(note)
	re.set_note(note)

func _on_dup_pressed():
	if re.note != null:
		var nt: MCIRDataPatternReelNote = re.note.dup_note()
		nt.start += nt.length
		re.reel.add_note(nt)
		re.set_note(nt)

func _on_delete_pressed():
	if not dg.pressed:
		return
	if re.note != null:
		re.reel.rm_note(re.note)
	re.set_note(null)

func _on_play_pressed():
	if re.note != null:
		pe.play_reel_note(re.reel, re.note)
