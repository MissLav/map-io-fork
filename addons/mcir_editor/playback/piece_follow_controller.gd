class_name MCIREditorPieceFollowController
extends MCIRExecAdvancable

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)

var time_left: int = 0
var piece: String = ""
var follow: bool = true
var done: bool = false

var held_patterns: Array = []

func mcir_advance_us(us: int):
	if done:
		return
	time_left -= us
	if time_left > 0:
		return
	done = true
	# do this first: release all held patterns
	for v in held_patterns:
		if is_instance_valid(v):
			var hp: MCIRExecPatternBase = v
			hp.mcir_exec_pattern_release()
	# next - marking for deletion makes sure that
	#  we don't count for the update trigger
	queue_free()
	# then do this to confirm that
	# can't directly reference the type though
	get_parent().piecefollowcontroller_done_hook()
	# and now optionally setup next one
	if follow:
		_setup_follow()

func _setup_follow():
	if not ep.circuit.pieces.has(piece):
		return
	var pc = ep.circuit.pieces[piece]
	var next = pc.editor_exit
	if next >= 0 and next < len(pc.connections):
		# cannot directly reference type!!!
		get_parent().play_piece(pc.connections[next], 0.0, true)
