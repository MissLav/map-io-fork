class_name MCIREditorBoolEditor
extends MCIREditorPropertyEditor

func _ready():
	connect("toggled", self, "set_property_value")

func set_ui_value(v: bool):
	self.pressed = v
