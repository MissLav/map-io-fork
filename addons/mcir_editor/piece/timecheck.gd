class_name MCIREditorTimeCheck
extends AudioStreamPlayer

var beats: Array

func _init():
	beats = []

func _process(_delta):
	if beats.size() == 0:
		if not playing:
			queue_free()
		return
	if OS.get_ticks_usec() >= beats[0]:
		beats.remove(0)
		play()
