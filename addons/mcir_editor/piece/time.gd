extends Node

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)
onready var pe: MCIREditorPlaybackExecutive = MCIREditorPlaybackExecutive.of(self)
onready var b_b: LineEdit = find_node("mcir_piecegraph_bpm")
onready var b_t: LineEdit = find_node("mcir_piecegraph_t")
onready var pnle: LineEdit = $hbc_xint/le
onready var sse: MCIREditorSSE = $sse

func _ready():
	b_b.connect("text_entered", self, "_time_text_edited")
	b_t.connect("text_entered", self, "_time_text_edited")
	sse.connect("user_edited", self, "_sse_user_edited")
	sse.connect("user_selected", self, "_suggestion_uptake")
	ep.connect("context_changed", self, "_context_changed")
	ep.connect("circuit_changed", self, "_context_changed")

func _suggestion_uptake(s):
	ep.pattern = s
	ep.exc()

func _context_changed():
	# easier to just reach in and do this
	pnle.text = ep.piece
	sse.suggest(ep.pattern)
	_circuit_changed()

func _circuit_changed():
	var pc = ep.get_current_piece()
	if pc is MCIRDataPiece:
		sse.set_value(pc.patterns)
		b_b.text = str(pc.bpm)
		b_t.text = str(pc.beats)
		b_b.editable = true
		b_t.editable = true
	else:
		sse.set_value(PoolStringArray())
		b_b.editable = false
		b_t.editable = false

func _sse_user_edited():
	var pc = ep.get_current_piece()
	if not pc is MCIRDataPiece:
		return
	pc.patterns = sse.value
	ep.ecc()

func _time_text_edited(_x):
	var pc = ep.get_current_piece()
	if not pc is MCIRDataPiece:
		return
	var did = false
	if b_b.text.is_valid_float():
		pc.bpm = float(b_b.text)
		did = true
	if b_t.text.is_valid_float():
		pc.beats = float(b_t.text)
		did = true
	if did:
		ep.ecc()

func _on_timecheck_pressed():
	_timechecker(false)

func _on_timecheck2_pressed():
	_timechecker(true)

func _timechecker(andplay: bool):
	var pc = ep.get_current_piece()
	if not pc is MCIRDataPiece:
		return
	var tc = MCIREditorTimeCheck.new()
	tc.stream = preload("beat.wav")
	var timebase = OS.get_ticks_usec()
	for i in range(pc.beats):
		tc.beats.push_back(timebase + pc.beat_time_usec(i))
	pe.inject(tc)
	if andplay:
		pe.play_piece(ep.piece, 0.0, false)
