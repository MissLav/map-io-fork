extends Node

onready var le: LineEdit = $le
onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)

func _new_piece(tie: bool):
	var c = ep.circuit
	var txt = le.text
	var counter = 0
	while check_piece_name_silent(txt) != "":
		counter += 1
		txt = "pc_" + str(counter)
	var existing = ep.get_current_piece()
	var dp: MCIRDataPieceBase
	if tie:
		dp = MCIRDataTie.new()
	else:
		dp = MCIRDataPiece.new()
	if existing != null:
		dp.editor_pos = existing.editor_pos + Vector2(20, 20)
		if existing is MCIRDataPiece and dp is MCIRDataPiece:
			dp.bpm = existing.bpm
			dp.beats = existing.beats
			dp.patterns = existing.patterns
	c.pieces[txt] = dp
	ep.piece = txt
	ep.ecxc()

func _on_new_pressed():
	_new_piece(false)

func _on_newtie_pressed():
	_new_piece(true)

func _on_rename_pc_pressed():
	var src = ep.piece
	var piece = ep.get_current_piece()
	if piece == null:
		ep.msgbox("Nothing selected!")
		return
	var dst = le.text
	var deleting = false
	var c = ep.circuit
	if dst == "":
		# if this piece connects to nothing, assume deletion.
		# 'deleting' consists of the piece becoming invisible,
		#  unless it's a tie.
		if not c.any_piece_connected_to(src):
			dst = "~" + str(OS.get_system_time_secs()) + "." + src
			deleting = true
		else:
			ep.msgbox("Won't do that!")
	if check_piece_name(dst):
		return
	for v in c.pieces.values():
		v.rename_connections(src, dst)
	# first-pass at deletion
	# if a tie, then stops
	if deleting:
		piece.editor_pos = Vector2.ZERO
		if not (piece is MCIRDataTie):
			deleting = false
	# this line prevents REAL deletion
	if not deleting:
		c.pieces[dst] = piece
	c.pieces.erase(src)
	ep.piece = dst
	ep.ecxc()

func check_piece_name(dst: String):
	var res = check_piece_name_silent(dst)
	if res != "":
		ep.msgbox(res)
		return true
	return false

func check_piece_name_silent(dst: String) -> String:
	if dst == "":
		return "piece name empty"
	if ep.circuit.pieces.has(dst):
		return "piece already with that name"
	return ""

func _on_le_text_entered(new_text):
	ep.piece = new_text
	ep.exc()
