class_name MCIREditorFloatEditor
extends MCIREditorPropertyEditor

func _ready():
	connect("text_entered", self, "_te")

func set_ui_value(v: float):
	self.text = str(v)

func _te(v: String):
	if not v.is_valid_float():
		return
	set_property_value(float(v))
