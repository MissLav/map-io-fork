class_name MCIREditorPropertyEditor
extends Node

export var t_holder_get = ""
export var t_holder_signal = ""
export var t_property = ""
export var t_holder_notify = ""
export var t_property_signal = ""

var holder: Object

var last_v: Object

func _ready():
	holder = _find_holder()
	if t_holder_signal != "":
		holder.connect(t_holder_signal, self, "_changed")
	_changed()

func _find_holder():
	var n = self
	while n != null:
		if n.has_method(t_holder_get):
			return n
		n = n.get_parent()

func _changed():
	if last_v != null:
		if t_property_signal != "":
			last_v.disconnect(t_property_signal, self, "_changed")
		last_v = null
	var v: Object = holder.call(t_holder_get)
	if v == null:
		return
	set_ui_value(v.get(t_property))
	if t_property_signal != "":
		v.connect(t_property_signal, self, "_changed")
	last_v = v

func set_ui_value(v):
	push_error("set_ui_value NYI")

func set_property_value(x):
	var v: Object = holder.call(t_holder_get)
	if v == null:
		return
	v.set(t_property, x)
	if t_holder_notify != "":
		holder.call(t_holder_notify)
