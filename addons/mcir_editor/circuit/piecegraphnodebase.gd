class_name MCIREditorPieceGraphNodeBase
extends GraphNode

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)
onready var pe: MCIREditorPlaybackExecutive = MCIREditorPlaybackExecutive.of(self)
var left_tracker: int = 0
var right_tracker: Array
var piece_base: MCIRDataPieceBase
var piece_name: String

func setup(pc, n: String):
	piece_base = pc
	piece_name = n
	offset = pc.editor_pos
	rect_size = get_minimum_size()
	right_tracker = []

func setup_slots():
	push_error("UNIMPLEMENTED: setup_slots")

func get_left_slot_index(i) -> int:
	push_error("UNIMPLEMENTED: get_left_slot_index")
	return 0

func get_right_slot_index(i) -> int:
	push_error("UNIMPLEMENTED: get_right_slot_index")
	return 0

func _ready():
	connect("offset_changed", self, "_on_GraphNode_offset_changed")
	pe.connect("playing_changed", self, "_playing_changed")
	_playing_changed()

func _playing_changed():
	if pe.is_playing_piece(piece_name):
		overlay = GraphNode.OVERLAY_POSITION
	else:
		overlay = GraphNode.OVERLAY_DISABLED

func _on_GraphNode_offset_changed():
	if piece_base == null:
		return
	piece_base.editor_pos = offset
