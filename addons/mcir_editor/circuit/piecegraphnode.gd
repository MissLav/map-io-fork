class_name MCIREditorPieceGraphNode
extends MCIREditorPieceGraphNodeBase

var piece: MCIRDataPiece

func setup(pc: MCIRDataPiece, n: String):
	title = n
	.setup(pc, n)
	piece = pc
	$hbc/spin.value = pc.editor_exit

func setup_slots():
	# get initial l/r slot counts
	var lc = left_tracker
	var rc = right_tracker.size()
	# there should be at least one slot on each side to make connections
	if lc == 0:
		lc = 1
	if rc == 0:
		rc = 1
	# add slots
	var total_slots = int(max(lc, rc))
	for i in range(total_slots):
		var si = _add_slot()
		var is_left = i < lc
		var is_right = i < rc
		set_slot(si, is_left, 0, Color.white, is_right, 0, Color.white)

func get_left_slot_index(i) -> int:
	return i

func get_right_slot_index(i) -> int:
	return i

func _add_slot():
	var si = get_child_count()
	var l = Label.new()
	l.text = ""
	add_child(l)
	return si

func _on_spin_value_changed(value):
	if ep != null:
		piece.editor_exit = value
		ep.ecc()

func _on_zero_pressed():
	if piece.editor_exit >= 0 and piece.editor_exit < len(piece.connections):
		var va = piece.connections
		var ex = va[piece.editor_exit]
		# remove from current place
		va.remove(piece.editor_exit)
		# create prepended
		var va2 = PoolStringArray([ex])
		va2.append_array(va)
		# apply prepended & set exit to 0
		piece.connections = va2
		piece.editor_exit = 0
		ep.ecc()
