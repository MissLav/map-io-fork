extends MCIREditorPieceGraphNodeBase

# these functions all do nothing, the tie is pre-set-up

func setup_slots():
	pass

func get_left_slot_index(i) -> int:
	return 0

func get_right_slot_index(i) -> int:
	return 0
