class_name MCIREditorPianoPlayManager
extends Node

onready var pe: MCIREditorPlaybackExecutive = MCIREditorPlaybackExecutive.of(self)

var piano_scancode_patterns: Dictionary

var current_pattern: String = ""

func _init():
	piano_scancode_patterns = {}

func _on_Piano_piano_key(idx, pressed):
	if pressed:
		# deter key repeat stupidity
		if not piano_scancode_patterns.has(idx):
			piano_scancode_patterns[idx] = pe.play_pattern(current_pattern, 0.0, idx)
	else:
		if piano_scancode_patterns.has(idx):
			var psp: MCIRExecPatternBase = piano_scancode_patterns[idx]
			if is_instance_valid(psp):
				psp.mcir_exec_pattern_release()
			piano_scancode_patterns.erase(idx)
