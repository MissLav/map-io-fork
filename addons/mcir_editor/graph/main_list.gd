extends VBoxContainer

onready var le: MCIREditorLinegraph = MCIREditorLinegraph.of(self)
onready var il: ItemList = $ItemList

func _ready():
	le.connect("target_changed", self, "_target_changed")

func _target_changed():
	il.clear()
	var lg = le.target
	if lg == null:
		return
	var time = 0.0
	var idx = 0
	for v in lg.lengths:
		var value = lg.values[idx]
		il.add_item(str(time) + "=" + str(value))
		idx += 1
		time += v
	if le.check_point_index():
		il.select(le.point_index)
		$lp.text = str(lg.lengths[le.point_index])
		$lv.text = str(lg.values[le.point_index])

func _opx(t):
	var lg = le.target
	if lg == null:
		return
	var pl = lg.lengths
	var pv = lg.values
	call(t, lg, pl, pv, le.point_index)

func _on_add_pressed():
	_opx("_add")

func _add(lg: MCIRDataLinegraph, pl: PoolRealArray, pv: PoolRealArray, pi: int):
	var v_v = 0.0
	if not le.check_point_index():
		pi = len(pl) - 1
	else:
		v_v = pv[pi]
	pl.insert(pi + 1, 1.0)
	pv.insert(pi + 1, v_v)
	le.point_index = pi + 1
	lg.update(pl, pv)

func _on_del_pressed():
	_opx("_del")

func _del(lg: MCIRDataLinegraph, pl: PoolRealArray, pv: PoolRealArray, pi: int):
	if not le.check_point_index():
		return
	pl.remove(pi)
	pv.remove(pi)
	lg.update(pl, pv)

func _on_up_pressed():
	_opx("_up")

func _up(lg: MCIRDataLinegraph, pl: PoolRealArray, pv: PoolRealArray, pi: int):
	if (le.point_index == 0) or not le.check_point_index():
		return
	var v_l = pl[pi]
	var v_v = pv[pi]
	pl.remove(pi)
	pv.remove(pi)
	pl.insert(pi - 1, v_l)
	pv.insert(pi - 1, v_v)
	le.point_index -= 1
	lg.update(pl, pv)

func _on_ItemList_item_selected(index):
	if index != le.point_index:
		le.point_index = index
		le.emit_signal("target_changed")

func _on_lp_text_entered(new_text: String):
	if not le.check_point_index():
		return
	if not new_text.is_valid_float():
		return
	var lg = le.target
	var pl = lg.lengths
	pl[le.point_index] = float(new_text)
	lg.update(pl, lg.values)

func _on_lv_text_entered(new_text: String):
	if not le.check_point_index():
		return
	if not new_text.is_valid_float():
		return
	var lg = le.target
	var pv = lg.values
	pv[le.point_index] = float(new_text)
	lg.update(lg.lengths, pv)
