class_name MCIREditorLinegraph
extends Node

var target: MCIRDataLinegraph
var point_index: int = 0
# fired on any change, including changed connection of target
# also on point index change, because REASONS
signal target_changed()

func check_point_index() -> bool:
	if target == null:
		return false
	return point_index < len(target.values)

func set_target(lg: MCIRDataLinegraph):
	if target != null:
		target.disconnect("changed", self, "_tcb")
	target = lg
	if target != null:
		target.connect("changed", self, "_tcb")
	emit_signal("target_changed")

func _tcb():
	emit_signal("target_changed")

static func of(n: Node) -> Node:
	while n != null:
		if n.has_method("_very_secret_mcireditorlinegraph_method"):
			return n
		n = n.get_parent()
	push_error("call to MCIREditorLinegraph.of did not work out")
	return null

func _very_secret_mcireditorlinegraph_method():
	pass

