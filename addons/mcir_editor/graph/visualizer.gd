extends Control

onready var le: MCIREditorLinegraph = MCIREditorLinegraph.of(self)

func _ready():
	le.connect("target_changed", self, "_target_changed")

func _target_changed():
	update()

func _draw():
	draw_rect(Rect2(Vector2.ZERO, rect_size), Color.black)
	var lg = le.target
	if lg == null:
		return
	# go over the lengths to get the full width to divide stuff by
	var mx = 0.0
	for v in lg.lengths:
		mx += v
	mx = max(1.0, ceil(mx))
	# go over the values to determine their range
	var vmx = 1.0
	var vmm = 0.0
	for v in lg.values:
		vmx = max(vmx, v)
		vmm = min(vmm, v)
	# transform
	var tf = Transform2D.IDENTITY
	tf = tf.scaled(Vector2(rect_size.x / mx, rect_size.y / (vmm - vmx)))
	tf = tf.translated(Vector2(0, -vmx))
	# draw grid lines
	for i in range(mx + 1):
		draw_line(tf * Vector2(i, vmm), tf * Vector2(i, vmx), Color.darkgray)
	for i in range(vmm, vmx):
		draw_line(tf * Vector2(0, i), tf * Vector2(mx, i), Color.darkgray)
	var vmid = (vmm + vmx) / 2
	draw_line(tf * Vector2(0, vmid), tf * Vector2(mx, vmid), Color.teal)
	# main line draw
	var idx = 0
	var ipt = 0.0
	var lv = 0.0
	var ll = 0.0
	for l in lg.lengths:
		var v = lg.values[idx]
		if idx != 0:
			draw_line(tf * Vector2(ipt - ll, lv), tf * Vector2(ipt, v), Color.white)
		idx += 1
		ipt += l
		lv = v
		ll = l

