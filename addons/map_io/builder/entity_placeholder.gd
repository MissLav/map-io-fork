class_name MapIOBuilderEntityPlaceholder
extends Spatial

# also for easy access
export var classname = "info_unknown"
# as node "model"
export var has_brushes = false
# for easy access
export var scale_divisor: float = 1.0
# MapIODataEntity
export var data: Resource
