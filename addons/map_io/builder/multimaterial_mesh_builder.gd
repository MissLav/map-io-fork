class_name MapIOBuilderMultimaterialMeshBuilder
extends Reference

const LAYER_NORMAL = 0
const LAYER_SHADOW = 1
const LAYER_COUNT = 2

# Maps materials (as material resources) to SurfaceTools.
var layers: Array

func _init():
	layers = [{}, {}]

func material(layer: int, m: Material) -> SurfaceTool:
	var l: Dictionary = layers[layer]
	if not l.has(m):
		var st = SurfaceTool.new()
		st.begin(Mesh.PRIMITIVE_TRIANGLES)
		st.set_material(m)
		l[m] = st
		return st
	return l[m]

func add_winding(winding: PoolVector3Array, uvs: PoolVector2Array, normal: Vector3, mat: MapIOBuilderMaterial):
	# actually build the face
	var normals = PoolVector3Array()
	for v in winding:
		normals.push_back(normal)
	if mat.material != null:
		var st = material(LAYER_NORMAL, mat.material)
		st.add_triangle_fan(winding, uvs, PoolColorArray(), PoolVector2Array(), normals)
	if mat.shadow_material != null:
		var st = material(LAYER_SHADOW, mat.shadow_material)
		st.add_triangle_fan(winding, uvs, PoolColorArray(), PoolVector2Array(), normals)

func commit(layer: int, lightmap_texel_size = 16.0) -> ArrayMesh:
	var am = ArrayMesh.new()
	var materials: Dictionary = layers[layer]
	var mats = materials.keys()
	for k in mats:
		var idx = am.get_surface_count()
		# print("Material " + str(k) + " @ " + str(idx))
		am.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, materials[k].commit_to_arrays())
	var i = 0
	for mat in mats:
		am.surface_set_material(i, mat)
		i += 1
	am.lightmap_unwrap(Transform.IDENTITY, lightmap_texel_size)
	return am

func commit_to_spatial(portal_mode, scale_divisor, use_in_baked_light: bool, generate_lightmap: bool, lightmap_texel_size) -> Spatial:
	var root = Spatial.new()
	var am1 = commit(LAYER_NORMAL, lightmap_texel_size)
	var ai1 = MeshInstance.new()
	ai1.name = "mesh_normal"
	ai1.portal_mode = portal_mode
	ai1.mesh = am1
	ai1.cast_shadow = GeometryInstance.SHADOW_CASTING_SETTING_OFF
	ai1.use_in_baked_light = use_in_baked_light
	ai1.generate_lightmap = generate_lightmap
	ai1.scale = Vector3.ONE / scale_divisor
	root.add_child(ai1)
	var am2 = commit(LAYER_SHADOW)
	var ai2 = MeshInstance.new()
	ai2.name = "mesh_shadow"
	ai2.portal_mode = portal_mode
	ai2.mesh = am2
	ai2.cast_shadow = GeometryInstance.SHADOW_CASTING_SETTING_SHADOWS_ONLY
	ai2.use_in_baked_light = use_in_baked_light
	ai2.generate_lightmap = false
	ai2.scale = Vector3.ONE / scale_divisor
	root.add_child(ai2)
	return root
