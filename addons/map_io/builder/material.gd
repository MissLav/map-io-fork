# This is a combination of a Godot Material and additional flags.
class_name MapIOBuilderMaterial
extends Resource

# Either of these can be null, which indicates the polygons should not even be generated.
export var material: Material
export var shadow_material: Material
export var size: Vector2 = Vector2.ONE
# If this is found on a surface of a brush, the brush doesn't get added to generated collision
export var no_collide: bool = false
# If true, this surface is deleted during surfacing.
export var skip: bool = false
# If true, partition connection can go through this surface.
export var window: bool = false
# This special flag marks the surface as an areaportal.
# In final assembly (builder_ll), areaportal surfaces are converted to Godot PVS portals.
# Note that a single-room build will still acknowledge areaportals.
# However it will delete those portals not exposed to the void.
export var areaportal: bool = false
# This special flag marks the surface as a tunnel.
# Tunnels are a mechanism designed pretty much to deal with room overlap errors.
# They connect different leaves - and therefore networks, and therefore rooms - together.
export var tunnel: bool = false

func autodetect_size():
	if material is SpatialMaterial:
		if material.albedo_texture != null:
			size = material.albedo_texture.get_size()
