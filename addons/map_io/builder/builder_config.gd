# High-level builder config.
class_name MapIOBuilderConfig
extends Resource

# -- Shared with low-level config --
export var scale_divisor = 64.0
# Worth noting: Origin brushes are handled in the brush entity builder.
# This is why the name assignment exists in the low level builder.
export var texture_origin = "tools/origin"
# --
export var texture_roots = PoolStringArray(["res://addons/map_io/textures/", "res://baseq3/textures/"])
export var texture_filename_extensions = PoolStringArray([".tres", ".res", ".material", ".png", ".jpg"])
export var lowercase_texture_names = false

# DO NOT refer to builder_config_ll here!
# it already has references here for conversion (copy_basics)
