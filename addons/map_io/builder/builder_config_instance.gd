class_name MapIOBuilderConfigInstance
extends MapIOBuilderConfigLL

# String -> MapIOBuilderMaterial
var material_cache: Dictionary

var ref_bc: MapIOBuilderConfig

# grabbed by the import plugin
var entity_converter: MapIOEntityConverter

func _init(from: MapIOBuilderConfigSpecific, basename: String):
	copy_basics(from)
	if from.portal_file:
		portal_file = basename + ".prt"
	if from.debug_info:
		debug_info = basename
	if ResourceLoader.exists(from.builder_config):
		ref_bc = load(from.builder_config)
	else:
		ref_bc = load(from.builder_config_fallback)
	copy_basics2(ref_bc)
	if ResourceLoader.exists(from.entity_config):
		entity_converter = load(from.entity_config)
	else:
		entity_converter = load(from.entity_config_fallback)
	material_lookup = funcref(self, "get_mat")
	material_cache = {}

func _setconv(v: PoolStringArray) -> Dictionary:
	var dict = {}
	for val in v:
		dict[val] = true
	return dict

func _texture_to_material(t: Texture) -> Material:
	var mx = SpatialMaterial.new()
	mx.albedo_texture = t
	# Would auto-detect transparency but chickening out because transparency now leads to BYPASS.
	return mx

func _material_to_bm(name: String, mat: Material) -> MapIOBuilderMaterial:
	var res = MapIOBuilderMaterial.new()
	res.material = mat
	res.shadow_material = mat
	res.autodetect_size()
	if mat is SpatialMaterial:
		if mat.params_use_alpha_scissor or mat.flags_transparent:
			# *gulp* hope it doesn't misdetect
			res.window = true
	return res

func _new_mat(mat: String) -> MapIOBuilderMaterial:
	# check for textures
	for root in ref_bc.texture_roots:
		for ext in ref_bc.texture_filename_extensions:
			if ResourceLoader.exists(root + mat + ext):
				var tex = load(root + mat + ext)
				if tex is MapIOBuilderMaterial:
					return tex
				if tex is Material:
					return _material_to_bm(mat, tex)
				if tex is Texture:
					return _material_to_bm(mat, _texture_to_material(tex))
				push_warning("MapIOBuilderConfig: Found potential texture " + root + mat + ext + " that was not a Texture, Material, or MapIOBuilderMaterial.")
	push_warning("MapIOBuilderConfig: Failed to find " + mat)
	return _material_to_bm(mat, preload("res://addons/map_io/textures/tools/error.tres"))

func get_mat(mat: String) -> MapIOBuilderMaterial:
	if ref_bc.lowercase_texture_names:
		mat = mat.to_lower()
	if material_cache.has(mat):
		return material_cache[mat]
	var rmat = _new_mat(mat)
	material_cache[mat] = rmat
	return rmat
