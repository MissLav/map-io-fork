# The map builder.
class_name MapIOBuilderLL
extends Reference

# Synced to mapio.fgd as "rendering" key
enum RENDERING_TYPE {
	DYNAMIC,
	WORLD_LIGHTING,
	NONE
}

# Surfaces is an array of MapIOSurfacingSurface.
static func build_single_room_core(surfaces: Array, config: MapIOBuilderConfigLL) -> Room:
	var room = Room.new()
	var r = build_rendering(surfaces, config, config.world_use_in_baked_light, config.world_generate_lightmap, CullInstance.PORTAL_MODE_STATIC)
	room.add_child(r)
	return room

# Called from:
#  entity_converter_simple.gd
static func attach_entity_data(root_x: Spatial, entity: MapIODataEntity, config: MapIOBuilderConfigLL):
	root_x.set_script(MapIOBuilderEntityPlaceholder)
	var root: MapIOBuilderEntityPlaceholder = root_x
	root.classname = entity.classname()
	root.has_brushes = not entity.brushes.empty()
	root.scale_divisor = config.scale_divisor
	root.data = entity

# Responsible for:
#  + Determining + applying Origin/Angles
#  + Rendering & Collision
# Essentially the core of the handling for a brush entity.
# Called from MapIOBuilder.build_entity_root
# rendering is RENDERING_TYPE
# collision is COLLISION_TYPE
static func build_entity(root: Spatial, entity: MapIODataEntity, config: MapIOBuilderConfigLL, rendering: int, collision: int, origin_guess_allowed: bool):
	var has_brushes = not entity.brushes.empty()
	var brush_model: Spatial = null
	if has_brushes and (rendering != RENDERING_TYPE.NONE):
		var surfaces = MapIOSurfacing.tagged_convexes_to_surfaces(MapIOSurfacing.brushes_to_tagged_convexes(entity.brushes), config)
		var bl = false
		var lm = false
		if rendering == RENDERING_TYPE.WORLD_LIGHTING:
			bl = config.world_use_in_baked_light
			lm = config.world_generate_lightmap
		brush_model = build_rendering(surfaces, config, bl, lm, CullInstance.PORTAL_MODE_ROAMING)
		root.add_child(brush_model)
	# determine origin
	var origin = determine_origin(entity, config, origin_guess_allowed)
	# origin/angle handling
	root.translation = origin
	root.rotation_degrees = entity.angles()
	var ora_inverse = root.transform.inverse()
	# brush model inversion handling
	if brush_model != null:
		brush_model.transform = ora_inverse
	# now for collision!
	var scale_transform = Transform.IDENTITY.scaled(Vector3.ONE / config.scale_divisor)
	var collision_transform = ora_inverse * scale_transform
	build_collision(root, entity.brushes, collision_transform, config, collision)

static func determine_origin(entity: MapIODataEntity, config: MapIOBuilderConfigLL, guess_allowed: bool) -> Vector3:
	if entity.keys.has("origin"):
		return entity.vector("origin") / config.scale_divisor
	# Pick the first brush as a fallback origin brush.
	# Then attempt to find a real origin brush.
	var origin_brush: MapIODataBrush = null
	if guess_allowed and (len(entity.brushes) > 0):
		origin_brush = entity.brushes[0]
	for v in entity.brushes:
		var brush: MapIODataBrush = v
		# Determine if brush contains any origin surfaces
		var has_origin: bool = false
		for tn in brush.textures:
			if tn == config.texture_origin:
				origin_brush = brush
				break
	# If the origin brush exists, use it.
	if origin_brush != null:
		var convex: MapIOMathsConvex = origin_brush.as_convex()
		return MapIOMathsBase.points_centre(convex.get_points()) / config.scale_divisor
	# No brushes and no explicit origin.
	return Vector3.ZERO

static func build_rendering(surfaces: Array, config: MapIOBuilderConfigLL, use_in_baked_light: bool, generate_lightmap: bool, portal_mode: int) -> Spatial:
	var portals = []
	var mb = MapIOBuilderMultimaterialMeshBuilder.new()
	for v in surfaces:
		var surface: MapIOSurfacingSurface = v
		if surface.material.areaportal:
			# Areaportal special stuff!
			# NOTES:
			# 1. This deliberately relies on the mesh-based portals conversion semantics.
			#    There is no other good way to do this.
			# 2. There's an implicit assumption here that the splitter won't create a wrongly-oriented areaportal.
			portals.push_back(build_portal(surface.winding, config.scale_divisor, portal_mode))
			pass
		else:
			surface.add_to_mesh_builder(mb)
	var brush_model = mb.commit_to_spatial(portal_mode, config.scale_divisor, use_in_baked_light, generate_lightmap, config.lightmap_texel_size)
	brush_model.name = "model"
	for portal in portals:
		brush_model.add_child(portal)
	return brush_model

static func build_portal(winding: PoolVector3Array, scale_divisor: float, pm: int) -> MeshInstance:
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	st.add_triangle_fan(winding)
	var mi = MeshInstance.new()
	mi.name = "*" + str(mi.get_instance_id()) + "-portal"
	mi.mesh = st.commit()
	mi.portal_mode = pm
	mi.scale = Vector3.ONE / scale_divisor
	# workaround that these are still visible
	mi.visible = false
	return mi

static func build_collision(parent: Spatial, brushes: Array, point_transform: Transform, config: MapIOBuilderConfigLL, collision_type: int):
	if collision_type == MapIOFGDEnums.COLLISION_TYPE.NONE:
		return
	for b in brushes:
		var brush: MapIODataBrush = b
		var no_collide = false
		for matname in brush.textures:
			var mat: MapIOBuilderMaterial = config.material_lookup.call_func(matname)
			if mat.no_collide:
				no_collide = true
				break
		if no_collide:
			continue
		# holds the Shape
		var shape
		var translation = Vector3.ZERO
		if collision_type == MapIOFGDEnums.COLLISION_TYPE.BOXES:
			shape = BoxShape.new()
			var points = brush.as_convex().get_points()
			var aabb: AABB = AABB(point_transform.xform(points[0]), Vector3(0, 0, 0))
			for p in points:
				aabb = aabb.expand(point_transform.xform(p))
			shape.extents = aabb.size / 2
			translation = aabb.position + shape.extents
		elif collision_type == MapIOFGDEnums.COLLISION_TYPE.CONCAVES:
			shape = ConcavePolygonShape.new()
			var convex = brush.as_convex()
			var tris = PoolVector3Array()
			for w in convex.windings:
				# convert winding to triangles!
				var pa = point_transform.xform(w[0])
				var i = 2
				while i < len(w):
					tris.push_back(pa)
					tris.push_back(point_transform.xform(w[i - 1]))
					tris.push_back(point_transform.xform(w[i]))
					i += 1
			shape.set_faces(tris)
		else:
			shape = ConvexPolygonShape.new()
			var points = brush.as_convex().get_points()
			var points2 = PoolVector3Array()
			for p in points:
				points2.push_back(point_transform.xform(p))
			shape.points = points2
		var cs = CollisionShape.new()
		cs.name = "brush"
		cs.shape = shape
		cs.translation = translation
		parent.add_child(cs)
