# The map builder.
class_name MapIOBuilder
extends Reference

static func build(map: MapIODataMap, config: MapIOBuilderConfigLL, converter: MapIOEntityConverter) -> Spatial:
	# Setup entity context
	var entity_context: MapIOEntityConverterContext = MapIOEntityConverterContext.new()
	entity_context.config = config
	# Handle entities (this accumulates context data, may be needed by later stages at some point)
	var entity_nodes = []
	config.profiler.profile_open("MapIOBuilder: entities")
	var i = 1
	while i < len(map.entities):
		var ent = map.entities[i]
		var entnode = converter._mapio_convert_entity(ent, entity_context)
		if entnode != null:
			entity_nodes.push_back(entnode)
		i += 1
	config.profiler.profile_close()
	# Prepare root
	var root: Spatial
	var worldspawn: MapIODataEntity = map.entities[0]
	if config.mode == MapIOBuilderConfigSpecific.MODE.PROP:
		# Props follow the default brush entity pipeline
		# However, origin guess is not allowed
		root = build_prop(worldspawn, config, config.prop_type, MapIOBuilderLL.RENDERING_TYPE.WORLD_LIGHTING, config.world_collision, false)
		# clear transform so that defaults are "honest"
		root.transform = Transform.IDENTITY
	elif config.mode == MapIOBuilderConfigSpecific.MODE.SINGLE_ROOM:
		# Single-room pipeline: Entities are part of the room for design simplicity,
		#  you're expected to extract them yourself
		root = build_single_room(worldspawn, config)
	elif config.mode == MapIOBuilderConfigSpecific.MODE.MULTI_ROOM:
		# Multi-room pipeline
		root = build_multi_room(worldspawn, config)
	elif config.mode == MapIOBuilderConfigSpecific.MODE.MULTI_ROOM_WITH_MANAGER:
		root = RoomManager.new()
		var roomlist = build_multi_room(worldspawn, config)
		roomlist.name = "rooms"
		root.add_child(roomlist)
		root.roomlist = "rooms"
	else:
		push_warning("MapIOBuilder: Unknown mode!")
		root = Spatial.new()
	# Add in entity nodes
	for v in entity_nodes:
		root.add_child(v)
	return root

static func build_single_room(worldspawn: MapIODataEntity, config: MapIOBuilderConfigLL) -> Room:
	var surfaces = _worldspawn_brushes_to_surfaces(worldspawn, config)
	var surfaces_void = []
	if not config.debug_disable_partitioning:
		var networks = _partition_and_portal_file(surfaces, config)
		var surfaces_dict = {}
		var surfaces_void_dict = {}
		config.profiler.profile_open("MapIOBuilder: room-building")
		for n in networks:
			var net: MapIOPartitioningNetwork = n
			if not net.touches_void:
				MapIOPartitioning.leaves_to_surface_dict(net.leaves, surfaces_dict)
			elif config.debug_include_void:
				MapIOPartitioning.leaves_to_surface_dict(net.leaves, surfaces_void_dict)
		config.profiler.profile_close()
		surfaces = surfaces_dict.keys()
		surfaces_void = surfaces_void_dict.keys()
	# Continue as normal
	var room = MapIOBuilderLL.build_single_room_core(surfaces, config)
	if config.debug_include_void and not config.debug_disable_partitioning:
		room.add_child(MapIOBuilderLL.build_single_room_core(surfaces_void, config))
	_attach_collision_and_data(room, worldspawn, config)
	return room

static func build_multi_room(worldspawn: MapIODataEntity, config: MapIOBuilderConfigLL) -> Spatial:
	var surfaces = _worldspawn_brushes_to_surfaces(worldspawn, config)
	# Partitioning section
	var networks = _partition_and_portal_file(surfaces, config)
	# Continue as normal
	var roomlist = Spatial.new()
	roomlist.set_display_folded(true)
	config.profiler.profile_open("MapIOBuilder: room-building")
	for n in networks:
		var net: MapIOPartitioningNetwork = n
		if (not net.touches_void) or config.debug_include_void:
			var local_surfaces = {}
			MapIOPartitioning.leaves_to_surface_dict(net.leaves, local_surfaces)
			roomlist.add_child(MapIOBuilderLL.build_single_room_core(local_surfaces.keys(), config))
	config.profiler.profile_close()
	# uuuuh...
	_attach_collision_and_data(roomlist, worldspawn, config)
	return roomlist

# Used for 
# Called from:
#  builder.gd (for props)
#  entity_converter_simple.gd (for entities that don't need special handling)
# body_type is BODY_TYPE
# rendering is RENDERING_TYPE
# collision is COLLISION_TYPE
static func build_prop(entity: MapIODataEntity, config: MapIOBuilderConfigLL, body_type: int, rendering: int, collision: int, origin_guess_allowed: bool):
	var root: Spatial
	if body_type == MapIOFGDEnums.BODY_TYPE.KINEMATIC:
		root = KinematicBody.new()
	elif body_type == MapIOFGDEnums.BODY_TYPE.KINEMATIC_SYNC_TO_PHYSICS:
		root = KinematicBody.new()
		root.set_sync_to_physics(true)
	elif body_type == MapIOFGDEnums.BODY_TYPE.STATIC:
		root = StaticBody.new()
	elif body_type == MapIOFGDEnums.BODY_TYPE.RIGID:
		root = RigidBody.new()
	elif body_type == MapIOFGDEnums.BODY_TYPE.RIGID_CHARACTER:
		root = RigidBody.new()
		root.mode = RigidBody.MODE_CHARACTER
	elif body_type == MapIOFGDEnums.BODY_TYPE.AREA:
		root = Area.new()
	else:
		if body_type != MapIOFGDEnums.BODY_TYPE.SPATIAL:
			push_warning("_mapio_convert_entity: found unknown body_type " + str(body_type))
		root = Spatial.new()
		collision = MapIOFGDEnums.COLLISION_TYPE.NONE
	MapIOBuilderLL.build_entity(root, entity, config, rendering, collision, origin_guess_allowed)
	if entity.keys.has("targetname"):
		root.name = entity.keys["targetname"]
	MapIOBuilderLL.attach_entity_data(root, entity, config)
	return root

static func _worldspawn_brushes_to_surfaces(worldspawn: MapIODataEntity, config: MapIOBuilderConfigLL):
	config.profiler.profile_open("MapIOBuilder: surfacing")
	var surfaces = MapIOSurfacing.tagged_convexes_to_surfaces(MapIOSurfacing.brushes_to_tagged_convexes(worldspawn.brushes), config)
	config.profiler.profile_close()
	return surfaces

static func _partition_and_portal_file(surfaces: Array, config: MapIOBuilderConfigLL):
	var leaves = MapIOPartitioning.partition(surfaces, config.profiler)
	if config.portal_file != "":
		config.profiler.profile_open("MapIOBuilder: portal file")
		MapIOMathsAux.save_windings_as_prt(config.portal_file, MapIOMathsAux.leaves_to_windings(leaves))
		config.profiler.profile_close()
	config.profiler.profile_open("MapIOBuilder: networking")
	var networks = MapIOPartitioning.leaves_to_networks(leaves)
	config.profiler.profile_close()
	if config.debug_info != "":
		config.profiler.profile_open("MapIOBuilder: network debug info")
		MapIOMathsAux.save_network_data(config.debug_info, networks)
		config.profiler.profile_close()
	return networks

static func _attach_collision_and_data(target: Spatial, worldspawn: MapIODataEntity, config: MapIOBuilderConfigLL):
	config.profiler.profile_open("MapIOBuilder: collision/data")
	MapIOBuilderLL.attach_entity_data(target, worldspawn, config)
	var staticbody = StaticBody.new()
	staticbody.set_display_folded(true)
	target.add_child(staticbody)
	MapIOBuilderLL.build_collision(staticbody, worldspawn.brushes, Transform.IDENTITY.scaled(Vector3.ONE / config.scale_divisor), config, config.world_collision)
	config.profiler.profile_close()
