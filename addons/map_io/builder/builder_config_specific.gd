# Highest-level builder config.
class_name MapIOBuilderConfigSpecific
extends Reference

# Remember: Keep the import plugin up to date with this!!!

enum MODE {
	SINGLE_ROOM, # a single Room w/ areaportals
	MULTI_ROOM, # split into rooms based on areaportals
	MULTI_ROOM_WITH_MANAGER, # MULTI_ROOM but with a RoomManager setup for maximum joy
	PROP # "prop" - compile "as if" via the default entity pipeline, see prop_type
}

const MODE_HINT = "Single Room,Multi-Room,Multi-Room With RoomManager,Prop"

# -- Shared with low-level & specific config --
var mode = MODE.MULTI_ROOM
var prop_type = MapIOFGDEnums.BODY_TYPE.STATIC
var world_collision = MapIOFGDEnums.COLLISION_TYPE.CONVEXES
var world_use_in_baked_light = true
var world_generate_lightmap = true
var lightmap_texel_size = 16.0
var debug_profile = false
var debug_include_void = false
var debug_disable_partitioning = false
# -- Handled by MapIOBuilderConfigInstance --
var builder_config = "res://default_map_io_config.tres"
var builder_config_fallback = "res://addons/map_io/config_copy_me/default_map_io_config.tres"
var entity_config = "res://default_map_io_entity_config.tres"
var entity_config_fallback = "res://addons/map_io/config_copy_me/default_map_io_entity_config.tres"
var portal_file = false
var debug_info = false
