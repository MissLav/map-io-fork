# Low-level builder config.
class_name MapIOBuilderConfigLL
extends Reference

# -- Shared with low-level & specific config --
var mode = MapIOBuilderConfigSpecific.MODE.MULTI_ROOM
var prop_type = MapIOFGDEnums.BODY_TYPE.STATIC
var world_collision = MapIOFGDEnums.COLLISION_TYPE.CONVEXES
var world_use_in_baked_light = true
var world_generate_lightmap = true
var lightmap_texel_size = 16.0
var debug_include_void = false
var debug_disable_partitioning = false
# -- Shared with high-level config --
var scale_divisor = 64.0
# Worth noting: Origin brushes are handled in the brush entity builder.
# This is why the name assignment exists in the low level builder.
var texture_origin = "tools/origin"
# --
var profiler: MapIOProfilerNull
var portal_file = ""
var debug_info = ""

# String -> MapIOBuilderMaterial (no nulls!)
# Mandatory to set!
var material_lookup: FuncRef

func copy_basics(from: MapIOBuilderConfigSpecific):
	mode = from.mode
	prop_type = from.prop_type
	world_collision = from.world_collision
	world_use_in_baked_light = from.world_use_in_baked_light
	world_generate_lightmap = from.world_generate_lightmap
	lightmap_texel_size = from.lightmap_texel_size
	debug_include_void = from.debug_include_void
	debug_disable_partitioning = from.debug_disable_partitioning
	if from.debug_profile:
		profiler = MapIOProfilerPrint.new()
	else:
		profiler = MapIOProfilerNull.new()

func copy_basics2(from: MapIOBuilderConfig):
	scale_divisor = from.scale_divisor
	texture_origin = from.texture_origin
