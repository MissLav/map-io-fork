class_name MapIOMapReader
extends Reference

# Will return null
static func read_map(txt: String) -> MapIODataMap:
	var map = MapIODataMap.new()
	var tokenizer: MapIOMapTokenizer = MapIOMapTokenizer.new()
	tokenizer.map = txt
	while true:
		if not tokenizer.read_token():
			break
		if tokenizer.is_symbol("{"):
			# Read entity!
			var entity = MapIODataEntity.new()
			while true:
				if not tokenizer.read_token():
					break
				if tokenizer.is_symbol("}"):
					# End of entity
					break
				elif tokenizer.is_symbol("{"):
					# Brush
					var brush = MapIODataBrush.new()
					while true:
						if not tokenizer.read_token():
							break
						if tokenizer.is_symbol("}"):
							# End of brush
							break
						elif tokenizer.is_symbol("("):
							# Start of face
							var point_a = tokenizer.quick_read_v3(")")
							point_a = MapIOMathsBase.conv_q2g(point_a)
							tokenizer.read_token()
							var point_b = tokenizer.quick_read_v3(")")
							point_b = MapIOMathsBase.conv_q2g(point_b)
							tokenizer.read_token()
							var point_c = tokenizer.quick_read_v3(")")
							point_c = MapIOMathsBase.conv_q2g(point_c)
							var plane = Plane(point_a, point_b, point_c)
							tokenizer.read_token()
							var texture = tokenizer.current_token_text
							tokenizer.read_token()
							if tokenizer.is_symbol("["):
								# Valve format
								var u = tokenizer.quick_read_vector("]")
								tokenizer.read_token()
								var v = tokenizer.quick_read_vector("]")
								tokenizer.read_token()
								var scale_u = tokenizer.read_float()
								var scale_v = tokenizer.read_float()
								brush.add_plane(plane, texture, u, v, scale_u, scale_v)
							else:
								# Quake format
								var offset_u = float(tokenizer.current_token_text)
								var offset_v = tokenizer.read_float()
								var angle = tokenizer.read_float()
								var scale_u = tokenizer.read_float()
								var scale_v = tokenizer.read_float()
								brush.add_old_plane(plane, texture, offset_u, offset_v, angle, scale_u, scale_v)
						# Note unhandled symbols are ignored,
						#  this is to eat extended (Q3) properties.
					entity.brushes.append(brush)
				else:
					# Property
					var key = tokenizer.current_token_text
					tokenizer.read_token()
					entity.keys[key] = tokenizer.current_token_text
			map.entities.append(entity)
		else:
			push_warning("Unexpected symbol { at " + tokenizer.report_location())
			return null
	return map
