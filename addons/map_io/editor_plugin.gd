tool
extends EditorPlugin

var map_importer
var map_importer_builder

func _enter_tree():
	map_importer = MapIOMapImportPlugin.new()
	map_importer_builder = MapIOMapImportPluginBuilder.new()
	add_import_plugin(map_importer)
	add_import_plugin(map_importer_builder)

func _exit_tree():
	remove_import_plugin(map_importer)
	remove_import_plugin(map_importer_builder)
	map_importer = null
	map_importer_builder = null

