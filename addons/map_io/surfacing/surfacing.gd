# Surfacing utilities
class_name MapIOSurfacing
extends Reference

# Since there's no carving stage, this acts as a placeholder
static func brushes_to_tagged_convexes(brushes: Array) -> Array:
	var tagged_convexes = []
	for b in brushes:
		tagged_convexes.push_back(b.as_convex())
	return tagged_convexes

static func tagged_convexes_to_surfaces(tagged_convexes: Array, config: MapIOBuilderConfigLL) -> Array:
	var surfaces = []
	for v in tagged_convexes:
		var convex: MapIOMathsConvex = v
		var i = 0
		while i < len(convex.windings):
			var tag = convex.winding_tags[i]
			if tag != null:
				var matname: String = tag[0].textures[tag[1]]
				var mat: MapIOBuilderMaterial = config.material_lookup.call_func(matname)
				if not mat.skip:
					var surf = MapIOSurfacingSurface.new(tag[0], tag[1], mat, convex.windings[i])
					surfaces.push_back(surf)
			i += 1
	return surfaces
