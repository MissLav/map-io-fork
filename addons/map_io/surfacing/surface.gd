# This is a surface - an abstraction used for building a mesh.
# Note that it should not be used for building collision.
class_name MapIOSurfacingSurface
extends Reference

var brush: MapIODataBrush
var face_index: int
var material: MapIOBuilderMaterial
var winding: PoolVector3Array
var winding_aabb: AABB
var importance_heuristic: float
var axis_aligned: bool

func _init(b: MapIODataBrush, f: int, m: MapIOBuilderMaterial, w: PoolVector3Array):
	brush = b
	face_index = f
	material = m
	winding = w
	winding_aabb = MapIOMathsBase.winding_aabb_expanded(w)
	# calculate sorting information
	importance_heuristic = winding_aabb.get_area()
	var normal: Vector3 = brush.normals[face_index]
	normal = normal.abs()
	axis_aligned = (normal.x == 1.0 or normal.y == 1.0 or normal.z == 1.0)

# finder is expected to return either a MapIOBuilderMaterial, or null.
func add_to_mesh_builder(builder: MapIOBuilderMultimaterialMeshBuilder):
	# actually build the face
	var uvs = brush.get_uvs(face_index, winding, material.size)
	builder.add_winding(winding, uvs, brush.normals[face_index], material)

func get_plane() -> Plane:
	return brush.get_plane(face_index)
