# Basic Usage Guide And Design Document

## Before anything else!

Take a good look at `config_copy_me`!

You can copy the `.tres` files to the root of your project to alter global MapIO settings.

## This needs a rewrite

This file needs a rewrite to be better-organized.

## Modes Of Operation

Various modes of operation exist depending on what you want to do.

You can:

1. Read MAP files and manipulate them directly, using classes as appropriate
2. Read MAP files and run them through the two conversion processes described below at runtime
3. Import a MAP file as a scene intended for use as a prop: Perform no BSP operations whatsoever. Intended solely for quickly making models out of brushwork!
4. Import a MAP file as a scene with a single room: Perform some basic BSP operations, but will treat all areaportals as pointing to other rooms.
5. Import a MAP file as a scene with multiple rooms: Essentially act as a semi-limited BSP compiler. Areaportals will be converted to actual PVS portals.

There are also variations upon this - the design is intended to allow "using what you want".

Some applications may not require the full capabilities, and other applications may require specific kinds of access to the MAP files, preprocessing steps, or even runtime generation.

Further, the mathematical primitives required may have uses by themselves.

## Entities

### New Users: Don't use entities!

For new users, it's expected that you'll probably want to load maps as scenes and do your entity/lighting placement in Godot.

The Godot editor is likely much better at live preview than your Quake map editor.

### Advanced Users: How to use entities

All non-worldspawn entities go through a subclass of `MapIOEntityConverter` (`entity_converter/entity_converter.gd`).

In practice, this will be `MapIOEntityConverterSimple` (`entity_converter/entity_converter_simple.gd`).

### Default Entity Handling

See `mapio.fgd` for what this means, but in short, it's a rough superset of the options you get for props.

What's added is:

+ The ability to create Areas (triggers, water)
+ The ability to choose the node name for reference by inherited scenes/etc.

## Texturing

Unless you directly pass a texture lookup function to MapIOBuilder, the method of texturing is based on a set of semicolon-separated "texture roots".

A "baseline" TrenchBroom configuration has been supplied that you can base your own configurations upon, along with a set of "base textures".

## Compiler Notes / Troubleshooting

MapIO uses a structure akin to a Quake BSP compiler in order to correctly segment the world into rooms.

+ Keep in mind the compiler can't detect a blocking surface unless that surface entirely covers the face of a leaf.
+ Keep in mind the BSP tree isn't actually used at runtime, because Godot PVS can't use it anyway. Therefore overtly complex BSP trees *only* affect compile-time.
+ Keep in mind that leaves with any outwards-facing surface that is not a window or an areaportal are automatically deleted.
+ Therefore: Don't misuse `tools/skip`! Only use it where it doesn't matter if the BSP tree is cut (i.e. backface of an areaportal, unwanted sides of a transparent surface).
+ `tools/hint` is a 100% safe replacement for `tools/skip`.
+ Keep in mind that the debug info also serves as a utility for debugging leaks.
+ `x.map.networking.map.prt` is a MAP file containing a visual representation of every leaf.
+ Once you've located the offending connecting leaves, you can lookup the leaf numbers in `x.map.networking.txt.prt` to determine the exact nature of the connection.
+ If "pocket" rooms are generated due to overlapping brushes, various options are available, but the one I particularly use is to 
+ Better solutions to "pocket" rooms are probably going to involve proper entity-based leak detection.
+ Because nodraw has to draw shadows, sky is cheaper than nodraw. (If nodraw didn't draw shadows, it'd be exactly the same as sky, and there'd be no way to quickly make an invisible shadowcaster.)

## Pipeline

1. Map Loading (`map_reader.gd`): Read MAP file into brushes. Internal conversion to Valve format would happen here (TODO, right now only handles Valve format).
2. Carving (TODO or may be omitted): Handle overlapping brushes.
3. Surfacing (`surfacing`): Convert brush faces to surfaces.
4. Partitioning (`partitioning/partitioning.gd`): Split the world into convex partitions using surfaces and assign surfaces to faces of these partitions.
5. Networking (`partitioning/partitioning.gd`): Create networks of connected partitions. This is implicitly areaportal-guided as areaportals block partitions.
6. Assembly/Meshgen (`builder/builder_ll.gd`): Bring it all together into more familiar Godot primitives.
7. Final Assembly (`builder/builder.gd`): Basically node rearrangement: arrange nodes in ways that are convenient for use in-engine.

