class_name MapIOPartitioningNetwork
extends Reference

var leaves: Array
# Maps areaportal surfaces to leaves. Useful for association between networks.
var areaportals: Dictionary
var touches_void: bool = false

func _init():
	leaves = []
	areaportals = {}
