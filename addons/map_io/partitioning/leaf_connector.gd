class_name MapIOPartitioningLeafConnector
extends Reference

var partitions: Array
var winding_lookup: MapIOMathsOpposingWindingLookup

func run():
	var connections = []
	_connect_leaves(partitions, connections)
	for connection in connections:
		var a: MapIOPartitioningLeaf = connection[0]
		var b: MapIOPartitioningLeaf = connection[2]
		a.connections[b.unique_id] = connection[1]
		b.connections[a.unique_id] = connection[3]

func _connect_leaves(set: Array, connections: Array):
	for pv in set:
		var partition: MapIOPartitioningLeaf = pv
		var j = 0
		while j < len(partition.convex.windings):
			# regular connections
			var ap = partition.convex.get_plane(j)
			var array = winding_lookup.find_opposing_windings(partition.convex.windings[j], ap)
			if array != null:
				for other in array:
					if _test_connect_leaves(partition, j, other[0], other[1], ap):
						connections.push_back([partition, j, other[0], other[1]])
			# tunnels
			var tag = partition.convex.winding_tags[j]
			if tag != null:
				for s in tag:
					var surf: MapIOSurfacingSurface = s
					if surf.material.tunnel:
						# tunnel brush - need to skip across to the other side
						_handle_tunnel_brush(connections, partition, j, surf.brush)
			j += 1

func _handle_tunnel_brush(connections: Array, leaf: MapIOPartitioningLeaf, leaf_winding: int, brush: MapIODataBrush):
	var convex = brush.as_convex()
	var i = 0
	while i < len(convex.windings):
		var winding = convex.windings[i]
		var array = winding_lookup.find_opposing_windings(convex.windings[i], convex.get_plane(i))
		if array != null:
			for other in array:
				var other_leaf: MapIOPartitioningLeaf = other[0]
				var other_winding = other_leaf.convex.windings[other[1]]
				if other_leaf != leaf and MapIOMathsBase.windings_intersect_noplane(winding, other_winding):
					connections.push_back([leaf, leaf_winding, other[0], other[1]])
		i += 1

func _test_connect_leaves(a: MapIOPartitioningLeaf, wia: int, b: MapIOPartitioningLeaf, wib: int, ap: Plane) -> bool:
	if a.unique_id <= b.unique_id:
		return false
	# Confirm that the two windings have a common point at all
	var wa: PoolVector3Array = a.convex.windings[wia]
	var wb: PoolVector3Array = b.convex.windings[wib]
	# need this intersection winding for surface applicability tests
	# note this isn't slower than the previous code because windings_intersect_noplane calls this anyway
	var wc: PoolVector3Array = MapIOMathsBase.windings_intersect_noplane_aswinding(wa, wb)
	if len(wc) == 0:
		return false
	# Alright, so the connection's mostly fine.
	# Check for surface block.
	# Note that the surface might not be shared between both leaves.
	# Check that, since it could mean we're looking at the wrong surface.
	for v in _common_blocking_surfaces(a, wia, b, wib):
		var surf: MapIOSurfacingSurface = v
		# it's important to note that the "combined winding" is the *actual* portal.
		# an implication of this is that the surface applicability test is all-or-nothing on that portal.
		if MapIOMathsBase.windings_contained_noplane(surf.winding, wc):
			return false
	return true

func _common_blocking_surfaces(a: MapIOPartitioningLeaf, wia: int, b: MapIOPartitioningLeaf, wib: int) -> Array:
	var at = a.convex.winding_tags[wia]
	var bt = b.convex.winding_tags[wib]
	if at == null or bt == null:
		return []
	var surfaces_a = {}
	var surfaces = {}
	for v in at:
		var surf: MapIOSurfacingSurface = v
		if not surf.material.window:
			surfaces_a[surf] = true
	for v2 in bt:
		var surf: MapIOSurfacingSurface = v2
		if not surf.material.window:
			if surfaces_a.has(v2):
				surfaces[surf] = true
	return surfaces.keys()
