class_name MapIOPartitioningLeaf
extends Reference

# Unique ID in leaf set.
# Used to loosely bind objects because of reference loops.
# Set in partitioning.gd just before committing to the partition list
var unique_id: int

# The convex describing the shape of this leaf.
# The tags are arrays with MapIOSurfacingSurfaces, or null (for facing void).
# The arrays are only properly reduced during reduce_winding_tags().
var convex: MapIOMathsConvex
# Surfaces that need to be split
var surfaces: Array
# Other connected leaves (unique IDs as keys, values = local winding IDs)
var connections: Dictionary

func _init(c: MapIOMathsConvex, s: Array):
	convex = c
	surfaces = s
	connections = {}

func touches_void() -> bool:
	for v in convex.winding_tags:
		if v == null:
			return true
	return false

func reduce_winding_tags():
	var i = 0
	while i < len(convex.winding_tags):
		var tag = convex.winding_tags[i]
		if tag != null:
			var results = []
			convex.winding_tags[i] = results
			for v in tag:
				var surf: MapIOSurfacingSurface = v
				if MapIOMathsBase.windings_intersect_noplane(surf.winding, convex.windings[i]):
					results.push_back(surf)
		i += 1
