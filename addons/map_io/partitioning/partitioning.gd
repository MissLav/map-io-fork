class_name MapIOPartitioning
extends Reference

# NOTE: This sorts the surfaces!
static func partition(surfaces: Array, profiler: MapIOProfilerNull) -> Dictionary:
	# 1. optimize surface order
	surfaces.sort_custom(MapIOPartitioningSurfaceOrderOptimizer, "sort_func")
	# 2. create partitions
	profiler.profile_open("MapIOPartitioning: creating leaves")
	var initial_convex = MapIOMathsConvex.new()
	initial_convex.init_bigcube()
	var initial_leaf = MapIOPartitioningLeaf.new(initial_convex, surfaces)
	var leaves = []
	_process_leaf(initial_leaf, leaves)
	# 3. plane lookup
	profiler.profile_close()
	profiler.profile_statistic("MapIOPartitioning: created " + str(len(leaves)) + " leaves")
	profiler.profile_open("MapIOPartitioning: creating winding lookup")
	var winding_lookup = MapIOMathsOpposingWindingLookupBoxes.new()
	_init_winding_lookup(winding_lookup, leaves)
	profiler.profile_close()
	profiler.profile_statistic("MapIOPartitioning: created winding lookup: " + winding_lookup.statistics())
	profiler.profile_open("MapIOPartitioning: connecting leaves")
	# 4. connections
	var connector = MapIOPartitioningLeafConnector.new()
	connector.partitions = leaves
	connector.winding_lookup = winding_lookup
	connector.run()
	profiler.profile_close()
	return leaves

static func _init_winding_lookup(winding_lookup: MapIOMathsOpposingWindingLookup, leaves: Array):
	for v in leaves:
		var leaf: MapIOPartitioningLeaf = v
		var i = 0
		while i < len(leaf.convex.windings):
			var plane = leaf.convex.get_plane(i)
			winding_lookup.add_winding(leaf.convex.windings[i], plane, [leaf, i])
			i += 1
	winding_lookup.compile()

static func _process_leaf(leaf: MapIOPartitioningLeaf, leaves: Array):
	# Determine if there's anything to do
	if len(leaf.surfaces) == 0:
		leaf.unique_id = len(leaves)
		# NOTE: There's a very, very good reason this check exists:
		# It's possible for leaves to be created inside a wall.
		# Those leaves could then become their own rooms (VERY BAD).
		# So to prevent this they're deleted now.
		# Doing this early also reduces the load on the connectivity code by over 50%!
		if not _is_walled_off(leaf):
			leaf.reduce_winding_tags()
			leaves.push_back(leaf)
		return
	# Split time.
	var split: MapIOSurfacingSurface = leaf.surfaces[0]
	leaf.surfaces.remove(0)
	var plane = split.get_plane()
	var plane_inv = -plane
	# Split surfaces between sides for proper plane propagation
	var surfaces_above = []
	var surfaces_below = []
	var surfaces_on = [split]
	for v in leaf.surfaces:
		var surf: MapIOSurfacingSurface = v
		var side = MapIOMathsBase.winding_plane_side(surf.winding, plane)
		if side == MapIOMathsBase.SIDE_ABOVE:
			surfaces_above.append(surf)
		elif side == MapIOMathsBase.SIDE_BELOW:
			surfaces_below.append(surf)
		else:
			# work out what to do with it
			var surf_plane = surf.get_plane()
			if surf_plane.is_equal_approx(plane) or surf_plane.is_equal_approx(plane_inv):
				# actually part of the surface
				surfaces_on.append(surf)
			else:
				# not part of the surface but crosses both sides
				surfaces_above.append(surf)
				surfaces_below.append(surf)
	# The ordering here is paramount, as cut_plane uses the below convex as the primary (modified) object.
	var convex_above = MapIOMathsConvex.new()
	var convex_below = leaf.convex
	convex_below.cut_plane(plane, surfaces_on, convex_above, surfaces_on.duplicate())
	# Determine what goes on as a result.
	# Note that either of these being empty is normal if a cut plane was used that never actually hits.
	# In this case the cut plane (surfaces_on surfaces) are presumably lost, but that's okay.
	# It didn't hit anyway, did it?
	# Note that surfaces_on should guarantee that "neighbour" planes are caught.
	if not convex_above.empty():
		_process_leaf(MapIOPartitioningLeaf.new(convex_above, surfaces_above), leaves)
	if not convex_below.empty():
		_process_leaf(MapIOPartitioningLeaf.new(convex_below, surfaces_below), leaves)

static func _is_walled_off(leaf: MapIOPartitioningLeaf) -> bool:
	var i = 0
	while i < len(leaf.convex.windings):
		var winding = leaf.convex.windings[i]
		var tag = leaf.convex.winding_tags[i]
		var plane = leaf.convex.get_plane(i)
		if tag == null:
			return false
		for v in tag:
			var surf: MapIOSurfacingSurface = v
			if not (surf.material.window or surf.material.areaportal):
				if surf.get_plane().is_equal_approx(plane):
					if MapIOMathsBase.windings_contained_noplane(surf.winding, winding):
						return true
		i += 1
	return false

static func leaves_to_networks(leaves: Array) -> Array:
	var networks = {}
	var checked = {}
	var check_queue = []
	for v in leaves:
		# Create a new network for each leaf.
		# Note that the queue ordering will guarantee no merges are required.
		check_queue.push_back([MapIOPartitioningNetwork.new(), v])
	while len(check_queue) > 0:
		# Grab entry from end
		var ptr = len(check_queue) - 1
		var cqe = check_queue[ptr]
		check_queue.remove(ptr)
		var network: MapIOPartitioningNetwork = cqe[0]
		var leaf: MapIOPartitioningLeaf = cqe[1]
		# Checked test
		if checked.has(leaf):
			continue
		checked[leaf] = true
		# Insert into network and confirm the network really exists
		network.leaves.push_back(leaf)
		networks[network] = true
		# Add in areaportals
		for tag in leaf.convex.winding_tags:
			if tag != null:
				for s in tag:
					var surface: MapIOSurfacingSurface = s
					if surface.material.areaportal:
						network.areaportals[surface] = leaf
		# Void test
		if leaf.touches_void():
			network.touches_void = true
		# Spread (New entries at end so they'll be processed first)
		for k in leaf.connections:
			check_queue.push_back([network, leaves[k]])
	print("MapIOPartitioning: " + str(len(networks.keys())) + " networks")
	return networks.keys()

# Note: Areaportal correction happens here
static func leaves_to_surface_dict(leaves: Array, surfaces: Dictionary):
	for v in leaves:
		var leaf: MapIOPartitioningLeaf = v
		var i = 0
		while i < len(leaf.convex.winding_tags):
			var tag = leaf.convex.winding_tags[i]
			if tag != null:
				for s in tag:
					var surface: MapIOSurfacingSurface = s
					# Areaportal check
					if surface.material.areaportal:
						if not surface.get_plane().is_equal_approx(leaf.convex.get_plane(i)):
							continue
					surfaces[s] = true
			i += 1
