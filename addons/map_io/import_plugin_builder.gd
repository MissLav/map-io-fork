tool
extends EditorImportPlugin
class_name MapIOMapImportPluginBuilder

func get_importer_name() -> String:
	return "mapio.map_file_builder"

func get_visible_name() -> String:
	return "MAP as Scene"

func get_save_extension() -> String:
	return "scn"

func get_resource_type() -> String:
	return "PackedScene"

func get_recognized_extensions() -> Array:
	return ["map"]

func get_preset_count():
	return 3

func get_preset_name(i):
	if i == 1:
		return "Room"
	if i == 2:
		return "Prop Static"
	return "World (Default)"

func get_import_options(i):
	var cfg = MapIOBuilderConfigSpecific.new()
	if i == 1:
		cfg.mode = MapIOBuilderConfigSpecific.MODE.SINGLE_ROOM
	elif i == 2:
		cfg.mode = MapIOBuilderConfigSpecific.MODE.PROP
	return [
		{"name": "collapse_func_groups", "default_value": true},
		{"name": "mode", "default_value": cfg.mode, "property_hint": PROPERTY_HINT_ENUM, "hint_string": MapIOBuilderConfigSpecific.MODE_HINT},
		{"name": "prop_type", "default_value": cfg.prop_type, "property_hint": PROPERTY_HINT_ENUM, "hint_string": MapIOFGDEnums.BODY_TYPE_HINT},
		{"name": "world_collision", "default_value": cfg.world_collision, "property_hint": PROPERTY_HINT_ENUM, "hint_string": MapIOFGDEnums.COLLISION_TYPE_HINT},
		{"name": "world_use_in_baked_light", "default_value": cfg.world_use_in_baked_light},
		{"name": "world_generate_lightmap", "default_value": cfg.world_generate_lightmap},
		{"name": "lightmap_texel_size", "default_value": cfg.lightmap_texel_size},
		{"name": "debug_profile", "default_value": cfg.debug_profile},
		{"name": "debug_include_void", "default_value": cfg.debug_include_void},
		{"name": "debug_disable_partitioning", "default_value": cfg.debug_disable_partitioning},
		{"name": "builder_config", "default_value": cfg.builder_config},
		{"name": "builder_config_fallback", "default_value": cfg.builder_config_fallback},
		{"name": "portal_file", "default_value": cfg.portal_file},
		{"name": "debug_info", "default_value": cfg.debug_info},
	]

func get_option_visibility(option, options):
	return true

func import(load_path: String, save_path: String, options, platform_variants, gen_files):
	# load config
	var config = MapIOBuilderConfigSpecific.new()
	for k in options:
		if k != "collapse_func_groups":
			config.set(k, options[k])
	# instance actual config
	var config2 = MapIOBuilderConfigInstance.new(config, load_path)
	# load map file
	var f: File = File.new()
	f.open(load_path, File.READ)
	var txt = f.get_as_text()
	config2.profiler.profile_open("MapIOMapImportPluginBuilder: reading map")
	var root = MapIOMapReader.read_map(txt)
	config2.profiler.profile_close()
	if root == null:
		push_error("MapIOMapImportPluginBuilder: Failed to load map!")
		return ERR_FILE_UNRECOGNIZED
	if options["collapse_func_groups"]:
		root.collapse_groups()
	# got the map...
	# and now the meat of the matter
	config2.profiler.profile_open("MapIOMapImportPluginBuilder: building")
	var node_root = MapIOBuilder.build(root, config2, config2.entity_converter)
	config2.profiler.profile_close()
	# finally, pack the resulting node
	_update_owners(node_root, node_root.get_children())
	var packed = PackedScene.new()
	var err = packed.pack(node_root)
	node_root.free()
	if err:
		return err
	# finish profile
	config2.profiler.profile_complete(load_path + ".speedscope.json.prt")
	return ResourceSaver.save(save_path + ".scn", packed)

func _update_owners(owner: Node, target: Array):
	for v in target:
		v.owner = owner
		_update_owners(owner, v.get_children())
