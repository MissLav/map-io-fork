class_name MapIOMapTokenizer
extends Reference

# Input buffer
var map: String = ""
var map_ptr: int = 0

var current_token_symbol: bool = false
var current_token_text: String = ""

# Returns true on EOF
func _seek_past_whitespace():
	# "GC" the string per-token
	map = map.substr(map_ptr)
	map_ptr = 0
	while map_ptr < len(map):
		if map.ord_at(map_ptr) > 32:
			# Comments
			if map.substr(map_ptr, 2) == "//":
				var ptr = map.find("\n", map_ptr)
				if ptr == -1:
					map_ptr = len(map)
					return true
				map_ptr = ptr + 1
			else:
				# Got something
				return false
		else:
			map_ptr += 1
	return true

# Returns false on EOF
func read_token() -> bool:
	if _seek_past_whitespace():
		return false
	var chr = map.ord_at(map_ptr)
	map_ptr += 1
	# It's worth noting we can't safely treat "{" as {.
	# There's a bunch of weird conventions in Half-Life slime textures and the like.
	# strings
	if chr == 34:
		current_token_symbol = false
		current_token_text = ""
		var escape = false
		while true:
			if map_ptr >= len(map):
				# just pretend it's all good
				break
			# note the type change here of chr, this is "fine"
			chr = map.substr(map_ptr, 1)
			map_ptr += 1
			if escape:
				current_token_text += chr
				escape = false
			elif chr == "\"":
				break
			elif chr == "\\":
				escape = true
			else:
				current_token_text += chr
		return true
	# long symbols, floats, etc.
	current_token_symbol = true
	# -1 for chr
	var cts = map_ptr - 1
	while map_ptr < len(map):
		if map.ord_at(map_ptr) <= 32:
			break
		map_ptr += 1
	current_token_text = map.substr(cts, map_ptr - cts)
	return true

func is_symbol(s: String) -> bool:
	return current_token_symbol and current_token_text == s

func quick_read_vector(t: String) -> PoolRealArray:
	_seek_past_whitespace()
	var idx = map.find(t, map_ptr)
	if idx == -1:
		return PoolRealArray()
	var prl = map.substr(map_ptr, idx - map_ptr).split_floats(" ", false)
	map_ptr = idx + 1
	return prl

func quick_read_v3(t: String) -> Vector3:
	var prl = quick_read_vector(t)
	return Vector3(prl[0], prl[1], prl[2])

func read_float() -> float:
	read_token()
	return float(current_token_text)

func report_location() -> String:
	return "Unknown location"
