class_name MapIOMathsConstants
extends Reference

# Epsilon constant used for a lot of stuff.
# It's worth noting that this is under the assumption of 32-bit floats.
const ON_EPSILON = 0.05

# AABB growth to ensure no zero-widths
const AABB_GROW = Vector3(1, 1, 1)
