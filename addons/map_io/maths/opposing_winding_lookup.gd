# Interface for winding acceleration structures.
# Note that:
# 1. The acceleration structure must never return a false positive in terms of the plane.
#    But it may return a false positive in terms of overlap.
# 2. The acceleration structure must find *opposing* windings.
#    That is, windings with the opposite plane to the given winding.
# 3. The acceleration structure must count any partial overlap.
#    It is not required to count "on-edge" overlap.
# 4. The planes supplied will always be the original plane for the winding.
#    That is, equivalent to winding_plane(winding).
class_name MapIOMathsOpposingWindingLookup
extends Reference

func add_winding(winding: PoolVector3Array, plane: Plane, value):
	push_error("Not Implemented")

func compile():
	push_error("Not Implemented")

func find_opposing_windings(winding: PoolVector3Array, plane: Plane) -> Array:
	push_error("Not Implemented")
	return []

func statistics() -> String:
	return "Not Implemented"
