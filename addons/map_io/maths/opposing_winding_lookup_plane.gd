# Naive implementation of plane-based winding lookup.
class_name MapIOMathsOpposingWindingLookupPlane
extends MapIOMathsOpposingWindingLookup

# List of planes.
var planes = []
# List of arrays.
var values = []

func _index_of_plane(p: Plane):
	var i = 0
	while i < len(planes):
		var pln: Plane = planes[i]
		if pln.is_equal_approx(p):
			return i
		i += 1
	return -1

func add_winding(winding: PoolVector3Array, plane: Plane, value):
	# Note the plane inversion occuring here.
	plane = -plane
	var pi = _index_of_plane(plane)
	if pi == -1:
		planes.push_back(plane)
		values.push_back([value])
	else:
		values[pi].push_back(value)

func compile():
	pass

func find_opposing_windings(winding: PoolVector3Array, plane: Plane) -> Array:
	var pi = _index_of_plane(plane)
	if pi == -1:
		return []
	else:
		return values[pi]

func statistics() -> String:
	return str(len(planes)) + " planes"
