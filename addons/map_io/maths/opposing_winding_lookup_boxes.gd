# Checks planes, then AABBs.
class_name MapIOMathsOpposingWindingLookupBoxes
extends MapIOMathsOpposingWindingLookup

var underlying: MapIOMathsOpposingWindingLookup

func _init():
	underlying = MapIOMathsOpposingWindingLookupPlane.new()

func add_winding(winding: PoolVector3Array, plane: Plane, value):
	underlying.add_winding(winding, plane, [MapIOMathsBase.winding_aabb_expanded(winding), value])

func compile():
	pass

func find_opposing_windings(winding: PoolVector3Array, plane: Plane) -> Array:
	var basis = underlying.find_opposing_windings(winding, plane)
	var target = MapIOMathsBase.winding_aabb_expanded(winding)
	var results = []
	for v in basis:
		var aabb: AABB = v[0]
		if aabb.intersects(target):
			results.push_back(v[1])
	return results

func statistics() -> String:
	return underlying.statistics() + " (wrapped w/ AABB check)"
