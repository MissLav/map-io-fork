# Utilities used by the other stuff
# Conceptual, but not code, credit goes to ID Software's Quake's POLYLIB.
class_name MapIOMathsBase
extends Reference

# Constant ordering here is important.
const SIDE_BELOW = -1
const SIDE_ON = 0
const SIDE_ABOVE = 1

# Convert Quake coordinates to Godot coordinates.
static func conv_q2g(x: Vector3) -> Vector3:
	return Vector3(x.x, x.z, -x.y)

# Convert Godot coordinates to Quake coordinates.
static func conv_g2q(x: Vector3) -> Vector3:
	return Vector3(x.x, -x.z, x.y)

static func point_plane_dot(pt: Vector3, p: Plane) -> float:
	return pt.dot(p.normal) - p.d

# returns a SIDE_* constant
static func dot_side(f: float) -> int:
	if f > MapIOMathsConstants.ON_EPSILON:
		return SIDE_ABOVE
	if f < -MapIOMathsConstants.ON_EPSILON:
		return SIDE_BELOW
	return SIDE_ON

static func unique_points(p: PoolVector3Array) -> PoolVector3Array:
	var res = PoolVector3Array()
	for v in p:
		var contained = false
		for v2 in res:
			var v2t: Vector3 = v2
			if v2t.distance_squared_to(v) < MapIOMathsConstants.ON_EPSILON:
				contained = true
				break
		if not contained:
			res.push_back(v)
	return res

# NOTE: Windings are oriented assuming Godot coordinates.
static func winding_plane(w: PoolVector3Array) -> Plane:
	return Plane(w[0], w[1], w[2])

static func winding_check(w: PoolVector3Array, nf: Vector3):
	for i in range(len(w) - 2):
		var p = Plane(w[i], w[i + 1], w[i + 2])
		assert(p.normal.is_equal_approx(nf))

# Splits up a winding with a plane.
# Returns an array: [above, below, cutpoints]
# Either above or below may be null.
# Cutpoints is made up of all points on the cutting plane.
# It will not return null (because it's still useful)
static func winding_split(w: PoolVector3Array, p: Plane) -> Array:
	# Lines
	var above = PoolVector3Array()
	var above_all_on = true
	var below = PoolVector3Array()
	var below_all_on = true
	var cutpoints = PoolVector3Array()
	var last_point: Vector3 = w[len(w) - 1]
	var last_point_side: int = dot_side(point_plane_dot(last_point, p))
	for this_point in w:
		var this_point_side: int = dot_side(point_plane_dot(this_point, p))
		# Determine what to do - there are 9 separate cases here,
		#  but not all have unique handling.
		# The most important thing to keep in mind is that we only care about this line,
		#  and to some extent only this point.
		# If a point is on a given side, it's already been placed on that side.
		# Thinking about this in detail, on intersect, both sides receive the intersect point.
		if this_point_side == SIDE_ABOVE:
			if last_point_side == SIDE_BELOW:
				# This=Above and Last=Below: Intersect
				var intersect = p.intersects_segment(this_point, last_point)
				below.push_back(intersect)
				above.push_back(intersect)
				cutpoints.push_back(intersect)
			# In any case, this point is valid if This=Above
			above.push_back(this_point)
			above_all_on = false
		elif this_point_side == SIDE_BELOW:
			if last_point_side == SIDE_ABOVE:
				# This=Below and Last=Above: Intersect
				var intersect = p.intersects_segment(this_point, last_point)
				below.push_back(intersect)
				above.push_back(intersect)
				cutpoints.push_back(intersect)
			# In any case, this point is valid if This=Below
			below.push_back(this_point)
			below_all_on = false
		else:
			# This=On
			# Last point doesn't matter since any intersection would always be here.
			above.push_back(this_point)
			below.push_back(this_point)
			cutpoints.push_back(this_point)
		# Update last point
		last_point = this_point
		last_point_side = this_point_side
	# Return results.
	# Note that it's assumed unnecessary points don't exist... hm.
	if len(above) < 3 or above_all_on:
		above = null
	if len(below) < 3 or below_all_on:
		below = null
	return [above, below, cutpoints]

static func winding_plane_side(w: PoolVector3Array, p: Plane) -> int:
	# "indeterminate"
	var side_so_far: int = -9
	for v in w:
		var side = dot_side(point_plane_dot(v, p))
		# SIDE_ON is effectively indeterminate unless either:
		# 1. all the points have it
		# 2. the points were on different sides
		if side != SIDE_ON:
			if side_so_far == -9:
				side_so_far = side
			elif side_so_far != side:
				# points on different sides
				side_so_far = SIDE_ON
				break
	if side_so_far == -9:
		return SIDE_ON
	return side_so_far

static func winding_from_points(points: PoolVector3Array, normal: Vector3) -> PoolVector3Array:
	var remainder: PoolVector3Array = points
	var winding = PoolVector3Array()
	# Grab the first point to start off with.
	winding.push_back(remainder[0])
	remainder.remove(0)
	var last_point: Vector3 = winding[0]
	while not remainder.empty():
		# We need to find the corresponding next cutpoint.
		var success: bool = false
		var i: int = 0
		while i < len(remainder):
			var this_point: Vector3 = remainder[i]
			# create the outwards plane
			var outwards_p_base = Plane(last_point, this_point, last_point + normal)
			# the + 0.125 acts as epsilon for the comparsion
			var outwards_p = Plane(outwards_p_base.normal, outwards_p_base.d + MapIOMathsConstants.ON_EPSILON)
			success = true
			# Have to check all points (not just ones left to go)
			for kp in points:
				if outwards_p.is_point_over(kp):
					success = false
					break
			if success:
				# Ok, add it
				winding.push_back(this_point)
				remainder.remove(i)
				last_point = this_point
				break
			i += 1
		if not success:
			printerr("cutpoint reconstruction failure. what did you *DO*?")
			# anything is better than nothing
			return points
	# Inconsistently produces backwards results, so fix them here.
	if Plane(winding[0], winding[1], winding[2]).normal.dot(normal) < 0.0:
		winding.invert()
	return winding

static func planes_inverse(a: Plane, b: Plane) -> bool:
	return (-a).is_equal_approx(b)

static func planes_match_or_inverse(a: Plane, b: Plane) -> bool:
	return a.is_equal_approx(b) or (-a).is_equal_approx(b)

static func windings_intersect_noplane(winding: PoolVector3Array, other: PoolVector3Array) -> bool:
	# EW EW EW EW EW!
	# Unfortunately this is the only way that actually works.
	return len(windings_intersect_noplane_aswinding(winding, other)) != 0

static func windings_intersect_noplane_aswinding(winding: PoolVector3Array, other: PoolVector3Array) -> PoolVector3Array:
	var other_normal = Plane(other[0], other[1], other[2]).normal
	var i: int = 0
	var last_point = other[len(other) - 1]
	while i < len(other):
		var this_point = other[i]
		var outwards_p = Plane(last_point, this_point, last_point + other_normal)
		var splitres = winding_split(winding, outwards_p)
		if splitres[1] == null:
			return PoolVector3Array()
		winding = splitres[1]
		last_point = this_point
		i += 1
	return winding

# Confirm that inner is contained within outer
static func windings_contained_noplane(outer: PoolVector3Array, inner: PoolVector3Array) -> bool:
	var outer_normal = Plane(outer[0], outer[1], outer[2]).normal
	var i: int = 0
	var last_point = outer[len(outer) - 1]
	while i < len(outer):
		var this_point = outer[i]
		var outwards_p = Plane(last_point, this_point, last_point + outer_normal)
		last_point = this_point
		for p in inner:
			var side = dot_side(point_plane_dot(p, outwards_p))
			if side > SIDE_ON:
				return false
		i += 1
	return true

static func winding_aabb_expanded(winding: PoolVector3Array) -> AABB:
	var aabb = AABB(winding[0] - MapIOMathsConstants.AABB_GROW, MapIOMathsConstants.AABB_GROW * 2)
	var i: int = 1
	while i < len(winding):
		aabb = aabb.merge(AABB(winding[i] - Vector3.ONE, Vector3.ONE * 2))
		i += 1
	return aabb

static func points_centre(points: PoolVector3Array) -> Vector3:
	var total: Vector3 = Vector3.ZERO
	var i: int = 0
	while i < len(points):
		total += points[i] / len(points)
		i += 1
	return total
