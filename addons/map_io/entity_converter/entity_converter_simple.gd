# Simple entity converter you probably want to use in most cases.
# Provides a sort of "set of entity paths" system with the same basic semantics as how materials work.
class_name MapIOEntityConverterSimple
extends MapIOEntityConverter
# I'm not sure why this needs to be marked as a tool script when almost every other class has not.
tool

func _mapio_convert_entity(entity: MapIODataEntity, context: MapIOEntityConverterContext) -> Spatial:
	return mapio_convert_entity_default(entity, context)

# Default entity handling. This is what MapIO will do with any entity that it doesn't understand.
static func mapio_convert_entity_default(entity: MapIODataEntity, context: MapIOEntityConverterContext) -> Spatial:
	var has_brushes = len(entity.brushes) > 0
	var body_type = MapIOFGDEnums.BODY_TYPE.SPATIAL
	var rendering = MapIOBuilderLL.RENDERING_TYPE.DYNAMIC
	var collision = MapIOFGDEnums.COLLISION_TYPE.CONVEXES
	if entity.keys.has("rendering"):
		rendering = int(entity.keys["rendering"])
	if has_brushes:
		body_type = MapIOFGDEnums.BODY_TYPE.KINEMATIC
		if entity.keys.has("body_type"):
			body_type = int(entity.keys["body_type"])
		if entity.keys.has("body_collision"):
			collision = int(entity.keys["body_collision"])
	return MapIOBuilder.build_prop(entity, context.config, body_type, rendering, collision, true)
