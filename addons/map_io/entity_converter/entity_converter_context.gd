# Contextual information for an entity conversion.
# The idea is that this is where information is placed to be sent to or received from other stages.
class_name MapIOEntityConverterContext
extends Reference

var config: MapIOBuilderConfigLL
