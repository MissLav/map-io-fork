# This is a base class on which you can write your own entity conversion logic.
class_name MapIOEntityConverter
extends Resource
# I'm not sure why this needs to be marked as a tool script when almost every other class has not.
tool

# Override me!
# Can return null, in which case no node is generated.
func _mapio_convert_entity(entity: MapIODataEntity, context: MapIOEntityConverterContext) -> Spatial:
	return null
