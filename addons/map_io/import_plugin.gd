tool
extends EditorImportPlugin
class_name MapIOMapImportPlugin

func get_importer_name() -> String:
	return "mapio.map_file"

func get_visible_name() -> String:
	return "MAP as MapIODataMap"

func get_save_extension() -> String:
	return "res"

func get_resource_type() -> String:
	return "Resource"

func get_recognized_extensions() -> Array:
	return ["map"]

func get_preset_count():
	return 1

func get_preset_name(i):
	return "Default"

func get_import_options(i):
	return [{"name": "collapse_func_groups", "default_value": true}]

func get_option_visibility(option, options):
	return true

func import(load_path: String, save_path: String, options, platform_variants, gen_files):
	var f: File = File.new()
	f.open(load_path, File.READ)
	var txt = f.get_as_text()
	var root = MapIOMapReader.read_map(txt)
	if options["collapse_func_groups"]:
		root.collapse_groups()
	if root != null:
		return ResourceSaver.save(save_path + ".res", root)
	else:
		push_error("MapIOMapImportPlugin: Failed to import map!")
		return ERR_FILE_UNRECOGNIZED
