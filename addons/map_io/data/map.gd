class_name MapIODataMap
extends Resource

# Array of MapIODataEntity
# Note that first entity is assumed to be worldspawn
export var entities: Array

func _init():
	entities = []

# Processing utilities

# Collapses groups, defining func_group as "groups", directly into worldspawn.
# This is default behaviour for import on the basis that editors use func_group,
#  and BSP compilers support func_group.
func collapse_groups():
	# Scan for func_groups
	var index_offset = 0
	for i in range(len(entities)):
		var entity: MapIODataEntity = entities[i + index_offset]
		if entity.classname() == "func_group":
			# Collapse brushes
			var worldspawn: MapIODataEntity = entities[0]
			worldspawn.brushes.append_array(entity.brushes)
			# Remove and shift
			entities.remove(i + index_offset)
			index_offset -= 1
