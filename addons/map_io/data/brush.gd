# Note that the texture details are specific to the map sub-format in use, and most are redundant.
# For now, Quake 3 (Valve) format is used.
class_name MapIODataBrush
extends Resource

export var normals: PoolVector3Array
export var distances: PoolRealArray
export var textures: PoolStringArray

# Array of Transform
export var uvs: Array

#   Y
#   |
#   +
#  / \ 
# Z   X

const UV_PLANE_ZY = Transform(Basis(
	#       X    Y   Z
	Vector3( 0,  0,  0), # X   Y
	Vector3( 0, -1,  0), # Y   |
	Vector3(-1,  0,  0)  # Z Z-+
), Vector3.ZERO)

const UV_PLANE_XZ = Transform(Basis(
	#       X    Y   Z
	Vector3( 1,  0,  0), # X +-X
	Vector3( 0,  0,  0), # Y |
	Vector3( 0,  1,  0)  # Z Z
), Vector3.ZERO)

const UV_PLANE_XY = Transform(Basis(
	#       X    Y   Z
	Vector3( 1,  0,  0), # X Y
	Vector3( 0, -1,  0), # Y |
	Vector3( 0,  0,  0)  # Z +-X
), Vector3.ZERO)

# Get the corresponding UV at a given point.
# Note that this maths should be tested with a Quake 3 (Valve) map as the reference.
# If something goes wrong elsewhere that's a matter of the conversion.
func get_uv(face: int, input: Vector3) -> Vector2:
	var tf: Transform = uvs[face]
	var output = tf.xform(input)
	return Vector2(output.x, output.y)

func get_uvs(face: int, winding: PoolVector3Array, size: Vector2) -> PoolVector3Array:
	var res = PoolVector2Array()
	for v in winding:
		res.push_back(get_uv(face, v) / size)
	return res

# Gets the plane of this face.
func get_plane(face: int) -> Plane:
	return Plane(normals[face], distances[face])

func add_plane(plane: Plane, texture: String, u: PoolRealArray, v: PoolRealArray, scale_u: float, scale_v: float):
	normals.append(plane.normal)
	distances.append(plane.d)
	textures.append(texture)
	# note the transform implicitly occurring here
	var tf = Transform(Basis(
		Vector3(u[0] / scale_u, v[0] / scale_v, 0),
		Vector3(u[2] / scale_u, v[2] / scale_v, 0),
		Vector3(-u[1] / scale_u, -v[1] / scale_v, 0)
	), Vector3(u[3], v[3], 0))
	uvs.push_back(tf)

func add_old_plane(plane: Plane, texture: String, offset_u: float, offset_v: float, angle: float, scale_u: float, scale_v: float):
	# Determine axis
	var axis = 1
	var nabs = plane.normal.abs()
	var cabs = nabs.y
	if nabs.x > nabs.y:
		axis = 0
		cabs = nabs.x
	if nabs.z > cabs:
		axis = 2
	# Axis
	var atf = UV_PLANE_ZY
	if axis == 1:
		atf = UV_PLANE_XZ
	elif axis == 2:
		atf = UV_PLANE_XY
	# This bit gets complicated
	var tfr = Transform.IDENTITY.rotated(Vector3(0, 0, 1), deg2rad(angle))
	# This bit also gets complicated, mainly owing to weirdness with Transform
	var tft = Transform(Basis(
		Vector3(1.0 / scale_u, 0, 0),
		Vector3(0, 1.0 / scale_v, 0),
		Vector3(0, 0, 0)
	), Vector3(offset_u, offset_v, 0))
	# Append
	normals.append(plane.normal)
	distances.append(plane.d)
	textures.append(texture)
	uvs.push_back(tft * tfr * atf)

# Get this brush as a MapIOMathsConvex. Planes are tagged with [self, i].
func as_convex() -> MapIOMathsConvex:
	var convex = MapIOMathsConvex.new()
	convex.init_bigcube()
	var i = 0
	while i < len(normals):
		convex.cut_plane(Plane(normals[i], distances[i]), [self, i])
		i += 1
	return convex
