class_name MapIODataEntity
extends Resource

# This maps strings to strings.
export var keys: Dictionary

# This is an array of MapIODataBrush.
export var brushes: Array

func _init():
	keys = {}
	brushes = []

func classname():
	if not keys.has("classname"):
		return ""
	return keys["classname"]

func vector(key: String) -> Vector3:
	if not keys.has(key):
		return Vector3.ZERO
	return vector_convert(keys[key])

func vector_nc(key: String) -> Vector3:
	if not keys.has(key):
		return Vector3.ZERO
	return vector_convert_nc(keys[key])

static func vector_convert(v: String) -> Vector3:
	return MapIOMathsBase.conv_q2g(vector_convert_nc(v))

static func vector_convert_nc(v: String) -> Vector3:
	var prl: PoolRealArray = v.split_floats(" ", false)
	if len(prl) < 3:
		return Vector3.ZERO
	return Vector3(prl[0], prl[1], prl[2])

func angles() -> Vector3:
	if keys.has("angle"):
		return Vector3(0, float(keys["angle"]), 0)
	return vector_nc("angles")
