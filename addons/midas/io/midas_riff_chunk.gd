extends Reference
class_name MidasRiffChunk

var chunk_id : int = 0
var data : PoolByteArray = PoolByteArray()
var ptr : int = 0

# It is assumed you have endianness set correctly for RIFF vs. MIDI
func read(f : File):
	ptr = 0
	var id : int = f.get_32() & 0xFFFFFFFF
	var ln : int = f.get_32() & 0xFFFFFFFF
	var d : PoolByteArray = f.get_buffer(ln)
	chunk_id = id
	data = d

func get_ub16() -> int:
	var i : int = data[ptr] << 8
	ptr += 1
	i |= data[ptr]
	ptr += 1
	return i

func get_sb16() -> int:
	var val : int = get_ub16()
	if val >= 0x8000:
		return val - 0x10000
	return val

func get_vli() -> int:
	var t : int = 0
	while true:
		var bv = data[ptr]
		ptr += 1
		t |= bv & 0x7F
		if bv & 0x80 == 0:
			break
		t <<= 7
	return t

func get_u8() -> int:
	var dp : int = data[ptr]
	ptr += 1
	return dp
