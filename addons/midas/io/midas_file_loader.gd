extends ResourceFormatLoader
class_name MidasFileLoader
tool

func get_recognized_extensions() -> PoolStringArray:
	return PoolStringArray(["mid"])

func get_resource_type(path : String) -> String:
	return "Resource"

func handles_type(typename : String):
	return typename == "Resource"

func load(path : String, original_path : String):
	var f = File.new()
	f.open(path, File.READ)
	var r = MidasFile.new()
	r.read(f)
	f.close()
	return r
