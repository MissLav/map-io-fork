extends Node
class_name MidasPlayer, "../icons/midas_player.png"

signal midi_event(event)

export var source : Resource
var event_ptr : Array = []
var now : float = 0.0

func _ready():
	var tracks_to_believe = len(source.tracks)
	if (source.mthd_format == MidasFile.MTHD_FORMAT_ONE_TRACK) or (source.mthd_format == MidasFile.MTHD_FORMAT_MULTI_SONG):
		tracks_to_believe = 1
	for i in range(0, tracks_to_believe):
		event_ptr.append(0)

func _process(delta):
	now += delta
	for i in range(0, len(event_ptr)):
		while true:
			if event_ptr[i] >= len(source.tracks[i]) - 1:
				break
			var cev = source.tracks[i][event_ptr[i]]
			if now < cev.time:
				break
			emit_signal("midi_event", cev.event)
			event_ptr[i] += 1
