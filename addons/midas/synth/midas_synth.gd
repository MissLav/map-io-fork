extends Node
class_name MidasSynth, "../icons/midas_synth.png"

var channel_progs : Dictionary

func _print_hex(pba : PoolByteArray):
	var ref : String = "0123456789ABCDEF"
	var s : String = ""
	for i in range(0, pba.size()):
		s += " " + ref[pba[i] >> 4] + ref[pba[i] & 0xF]
	print(s)

# Contents are [channel][note] = KAuMidasNote
var sounds : Dictionary = {}
# Contents are [channel] = Node
var programs : Dictionary = {}

# Contents are [channel] = int
var bend_raw : Dictionary = {}
# Contents are [channel] = int
var vol_raw : Dictionary = {}
# Contents are [channel] = int
var exp_raw : Dictionary = {}
# Contents are [channel] = int
var pan_raw : Dictionary = {}

var programs_raw : Dictionary = {}
# Contents are [program] = Node
var program_nodes : Dictionary = {}

export var solo: int = -1
export var bus: String = "Master"

onready var note_holder = get_node("Notes")

export var show_warnings: bool = true

func _ready():
	program_nodes[255] = get_node("Font").get_node_or_null("Percussion")
	for nt in get_node("Font").get_children():
		if nt.name != "Percussion":
			program_nodes[int(nt.name.split("-")[0])] = nt

func _get_channel_program(channel : int):
	if channel == 9:
		return program_nodes[255]
	if programs.has(channel):
		return programs[channel]
	else:
		if solo != -1:
			return null
		return program_nodes[0]

func _conv_bend(channel: int):
	if not bend_raw.has(channel):
		return 0
	return (bend_raw[channel] - 8192) / 8192.0

func _conv_cvol(channel: int):
	var pvol: float = 1
	if pan_raw.has(channel):
		# Even if you don't have a way to do stereo, you have to implement pan, because it affects volume
		pvol = 1 - abs((pan_raw[channel] - 64) / 128.0)
	if vol_raw.has(channel):
		pvol *= vol_raw[channel] / 64.0
	if exp_raw.has(channel):
		pvol *= exp_raw[channel] / 64.0
	return pvol

func _apply_channel_cc_to(channel: int, nt: MidasSynthPlayingNote):
	nt.apply_cc(_conv_bend(channel), _conv_cvol(channel))

func _apply_channel_cc(channel: int):
	var bnd = _conv_bend(channel)
	var cvl = _conv_cvol(channel)
	if sounds.has(channel):
		for note in sounds[channel]:
			sounds[channel][note].apply_cc(bnd, cvl)

func note_off(channel : int, pitch : int):
	var nam = str(channel) + "." + str(pitch)
	if sounds.has(channel):
		if sounds[channel].has(pitch):
			sounds[channel][pitch].off()
			sounds[channel].erase(pitch)

func note_on(channel : int, pitch : int, velocity : int):
	note_off(channel, pitch)
	var prog = _get_channel_program(channel)
	if prog != null:
		var nt : MidasSynthPlayingNote = MidasSynthPlayingNote.new()
		nt.bus = bus
		nt.from_layers(prog.get_children(), pitch, velocity)
		if show_warnings and len(nt.notes) == 0:
			print("MIDas: " + str(channel) + " / " + str(pitch) + " resulted in no sound")
		_apply_channel_cc_to(channel, nt)
		if not sounds.has(channel):
			sounds[channel] = {}
		sounds[channel][pitch] = nt
		note_holder.add_child(nt)

func midi_event(ev: PoolByteArray):
	#_print_hex(ev)
	var channel = ev[0] & 0xF
	# With the exception of system commands (0xF0-0xFF)
	var cmd = ev[0] & 0xF0
	if cmd == 0x80:
		# Note off.
		note_off(channel, ev[1])
	elif cmd == 0x90:
		# Note on.
		if ev[2] != 0:
			note_on(channel, ev[1], ev[2])
		else:
			note_off(channel, ev[1])
	elif cmd == 0xB0:
		# CC change.
		if ev[1] == 7:
			vol_raw[channel] = ev[2]
			_apply_channel_cc(channel)
		elif ev[1] == 10:
			pan_raw[channel] = ev[2]
			_apply_channel_cc(channel)
		elif ev[1] == 11:
			exp_raw[channel] = ev[2]
			_apply_channel_cc(channel)
		else:
			pass
			#print("WARNING: GOT CC " + str(ev[1]) + " (NYI)")
	elif cmd == 0xC0:
		# Program change.
		programs_raw[channel] = ev[1]
		if (solo != -1) and (solo != ev[1]):
			programs.erase(channel)
		elif program_nodes.has(ev[1]):
			programs[channel] = program_nodes[ev[1]]
		else:
			programs.erase(channel)
	elif cmd == 0xE0:
		# Pitch bend.
		bend_raw[channel] = (ev[2] << 7) | ev[1]
		_apply_channel_cc(channel)
