extends Node
class_name MidasSynthPlayingNote

var notes: Array = []
var bus: String = "Master"
var abandoned: bool = false
var did_queue: bool = false

func from_layers(layers: Array, n: int, v: int):
	for m in layers:
		var mt : MidasSynthLayer = m
		if mt.should_play_for(n):
			var asp = MidasSynthPlayingLayer.new(mt, n, v)
			asp.bus = bus
			add_child(asp)
			notes.append(asp)
	return len(notes)

func apply_cc(bend_ofs, v):
	for n in notes:
		if n.bent != bend_ofs:
			n.bent = bend_ofs
			n.update_pitch()
		if n.channel_volume != v:
			n.channel_volume = v
			n.update_volume()

func off():
	for n in notes:
		n.off()
	abandoned = true

func _process(delta):
	if abandoned:
		var ok : bool = false
		for n in notes:
			if n != null:
				if n.playing or n.envelope_state == MidasSynthPlayingLayer.ENVELOPE_DELAY:
					ok = true
					break
		if not ok:
			if not did_queue:
				set_process(false)
				# Setting pause_mode followed by queue_delete here directly seems to cause a race condition.
				# Let's not do that... instead, queue_free.
				queue_free()
				did_queue = true
