extends Node
class_name MidasSynthLayer, "../icons/midas_synth_layer.png"

# Global volume multiplier.
# First for very quick tuning.
export var root_volume : float = 1.0

# Delay before this layer starts.
export var delay : float

# Expected to be rated for 440hz.
export var patch_440hz : AudioStreamSample
# Tuning in semitones.
export var tune_note_in : float = 0.0
# Tuning in direct output hertz control.
export var tune_note_out : float = 0.0
# Sweep in semitones-per-second.
export var sweep : float = 0
# Initial sweep in semitones.
export var sweep_init : float = 0
# Stop sweep when it hits zero.
export var halt_sweep_at_zero : bool = false

export var vibrato_mul : float = 0
export var vibrato_freq_mul : float = 1

export var specific_notes : Array = []
export var static_pitch : bool = false

# Stick with these numbers and don't change them because they propagate
export var attack_time: float = 0.016
export var decay_time: float = 2.75
export var sustain_volume: float = 0.75
export var release_time: float = 0.125

# Disables the envelope generator.
export var disable_envelope : bool = false
# Instantly releases envelope.
export var instant_release : bool = false

const NOTE_440: int = 69

var cached_df: AudioStreamSample

func midi_to_freq_core(note : float):
	return 27.5 * pow(2, ((note - 21) / 12.0))
	
func midi_to_freq(note : float):
	return midi_to_freq_core(note + tune_note_in) + tune_note_out

func get_sample() -> AudioStreamSample:
	if disable_envelope:
		return patch_440hz
	if cached_df != null:
		return cached_df
	# Ok, so, I'm not sure what happened here.
	# Apparently past-me thought Godot was going wrong.
	# The actual oversight was that loop_end is in samples, not bytes.
	# As for why this code even messes about with loops,
	#  it's because if this code doesn't mess about with loops,
	#  then the files all have to be set to loop manually.
	var ns : AudioStreamSample = AudioStreamSample.new()
	ns.format = patch_440hz.format
	ns.stereo = patch_440hz.stereo
	ns.mix_rate = patch_440hz.mix_rate
	var np : PoolByteArray = PoolByteArray()
	np.append_array(patch_440hz.data)
	ns.data = np
	ns.loop_mode = AudioStreamSample.LOOP_FORWARD
	ns.loop_begin = 0
	# calc sample size
	var sample_size = 1
	if ns.format == AudioStreamSample.FORMAT_8_BITS:
		pass
	elif ns.format == AudioStreamSample.FORMAT_16_BITS:
		sample_size = 2
	else:
		push_error("looped sample size not calculatable for this format")
	if ns.stereo:
		sample_size *= 2
	# ok, now set this
	ns.loop_end = patch_440hz.data.size() / sample_size
	cached_df = ns
	return ns

# Since these are used as a *reference*, tuning isn't supposed to apply here.
# Applying tuning would *almost* make it balance out, but not quite, making weird - and unwanted - effects.
func get_sample_base_freq() -> int:
	return midi_to_freq_core(NOTE_440)

func should_play_for(n : int):
	if len(specific_notes) > 0:
		return specific_notes.find(n) != -1
	return true
