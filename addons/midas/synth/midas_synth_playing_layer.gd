extends AudioStreamPlayer
class_name MidasSynthPlayingLayer

var layer : MidasSynthLayer

var note : int = 0
var velocity_vol : float = 0

# stuff controlled externally
var bent : float = 0
var channel_volume: float = 1

var envelope_volume : float = 0.0
var envelope_atkspd : float = 0.0
var envelope_decspd : float = 0.0
var envelope_relspd : float = 0.0

var sweep_progress : float = 0.0
var vibrato_progress : float = 0.0
var delay_progress : float = 0.0

# Applies even with envelope disabled
const ENVELOPE_DELAY = -1
const ENVELOPE_ATTACK = 0
const ENVELOPE_DECAY = 1
const ENVELOPE_RELEASE = 2
var envelope_state : int = ENVELOPE_DELAY

const ATTACK_VOL: float = 1.0

# Experimental, might not even do it's job
const LOW_PITCH_POW: float = 2.0
const LOW_PITCH_POW_MUL: float = 0.5
var pitch_vol_mul: float = 1

const TRUST_VELOCITY_MUL: float = 1.0

# Used for pitch-bending unless static_pitch is set.
var the_base_freq : int = 0

# Undo DB madness! I don't care if audio engineers say it's better, I care that this code has to work
#  with ordinary ADSR envelopes.
func _scale_to_db(f : float):
	return log(f) * 8.6561702453338

func _calc_speed(time : float, ff : float, ft : float):
	if time == 0:
		return INF
	return (ft - ff) / time

func _init(l : MidasSynthLayer, n : int, v : int):
	velocity_vol = (((v / 127.0) - 0.5) * TRUST_VELOCITY_MUL) + 0.5
	layer = l
	if layer.static_pitch:
		note = MidasSynthLayer.NOTE_440
	else:
		note = n
	# GNU Octave:
	#  range = 0:127 ; plot(range, ((((2 - (range / 64)) .^ LOW_PITCH_POW) - 1) * LOW_PITCH_POW_MUL) + 1)
	pitch_vol_mul = ((pow((2 - (note / 64.0)), LOW_PITCH_POW) - 1) * LOW_PITCH_POW_MUL) + 1
	sweep_progress = l.sweep_init
	stream = layer.get_sample()
	the_base_freq = layer.get_sample_base_freq()
	if not layer.disable_envelope:
		envelope_atkspd = _calc_speed(layer.attack_time, 0.0, ATTACK_VOL)
		envelope_decspd = _calc_speed(layer.decay_time, ATTACK_VOL, layer.sustain_volume)
		# Done based on attack volume because sometimes sustain volume may be "eventually zero",
		#  while release may be "zero now"
		envelope_relspd = _calc_speed(layer.release_time, ATTACK_VOL, 0.0)
		if layer.attack_time == 0.0:
			envelope_volume = ATTACK_VOL
	# Initialize
	update_pitch()
	update_volume()

func update_pitch():
	pitch_scale = layer.midi_to_freq(note + (layer.vibrato_mul * sin(vibrato_progress)) + sweep_progress + (bent * 2)) / the_base_freq

func update_volume():
	var ext_vol: float = layer.root_volume * channel_volume * pitch_vol_mul
	if layer.disable_envelope:
		#print("DE " + str(note))
		volume_db = _scale_to_db(layer.sustain_volume * velocity_vol * ext_vol)
	else:
		# Envelope needs to initialize. Velocity is applied in the envelope calculator in _process.
		volume_db = _scale_to_db(envelope_volume * velocity_vol * ext_vol)

func off():
	if not layer.instant_release:
		if envelope_state == ENVELOPE_ATTACK:
			# workaround in case of anything which instantly releases notes
			envelope_volume = ATTACK_VOL
		envelope_state = ENVELOPE_RELEASE

func _ready():
	_physics_process(0)
	set_physics_process((not layer.disable_envelope) or (layer.sweep != 0) or (layer.vibrato_freq_mul != 0) or (layer.delay > 0))

func _physics_process(delta):
	if envelope_state == ENVELOPE_DELAY:
		delay_progress += delta
		if delay_progress < layer.delay:
			return
		envelope_state = ENVELOPE_ATTACK
		play()
	if (layer.sweep != 0) or (layer.vibrato_mul != 0):
		sweep_progress += delta * layer.sweep
		vibrato_progress += delta * layer.vibrato_freq_mul * 3.14159
		if layer.halt_sweep_at_zero:
			if layer.sweep > 0:
				if sweep_progress >= 0:
					sweep_progress = 0
			else:
				if sweep_progress <= 0:
					sweep_progress = 0
		update_pitch()
	if not layer.disable_envelope:
		_update_envelope_core(delta)
		update_volume()

func _update_envelope_core(delta : float):
	if envelope_state == ENVELOPE_ATTACK:
		if envelope_atkspd == INF:
			envelope_volume = ATTACK_VOL
			envelope_state = ENVELOPE_DECAY
		else:
			envelope_volume += envelope_atkspd * delta
			if envelope_volume >= ATTACK_VOL:
				envelope_volume = ATTACK_VOL
				envelope_state = ENVELOPE_DECAY
	if envelope_state == ENVELOPE_DECAY:
		# it's important that instant-release take priority
		# because harps are too fast for this to work properly
		if layer.instant_release:
			envelope_state = ENVELOPE_RELEASE
		elif envelope_decspd == INF:
			envelope_volume = layer.sustain_volume
		else:
			envelope_volume += envelope_decspd * delta
			if envelope_volume <= layer.sustain_volume:
				envelope_volume = layer.sustain_volume
	if envelope_state == ENVELOPE_RELEASE:
		if envelope_relspd == INF:
			envelope_volume = 0.0
			stop()
		else:
			envelope_volume += envelope_relspd * delta
			if envelope_volume <= 0.0:
				envelope_volume = 0.0
				stop()
