class_name MCIRExecEnvironmentalData
# Not a Resource, you aren't supposed to be saving these.
extends Reference

# global
var circuit: MCIRDataCircuit
var audio_bus = "Master"
var enable_recursion_check: bool = true
var pattern_stack: PoolStringArray = PoolStringArray()

# local
# Start position in seconds
var start_pos = 0.0
var ival: MCIRExecAftertouchState

func _init():
	ival = MCIRExecAftertouchState.new()

func copy(src):
	start_pos = src.start_pos
	ival.copy(src.aftertouch)
	copy_global(src)

func copy_global(src):
	circuit = src.circuit
	audio_bus = src.audio_bus
	enable_recursion_check = src.enable_recursion_check
	if enable_recursion_check:
		pattern_stack = src.pattern_stack

func copy_global_add(src, ptn: String) -> bool:
	copy_global(src)
	if enable_recursion_check:
		for v in pattern_stack:
			if v == ptn:
				print("MCIR recursive pattern error on " + ptn)
				return false
		pattern_stack.push_back(ptn)
	return true
