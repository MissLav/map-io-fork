# Only used by the editor, kept here for code separation reasons,
#  since this relies on 'internal details'.
class_name MCIRExecPatternReelNoteQADSelfTimed
extends MCIRExecPatternReelNote

# Amount of time of a beat in seconds.
var st_beat_time: float = 1.0

func mcir_advance_us(us: int):
	var delta = float(us) / 1000000.0
	set_relative_beat(relative_beat + (delta / st_beat_time))
	.mcir_advance_us(us)
