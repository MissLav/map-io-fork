extends MCIRExecPatternBase

# This is pretty much finalized.
# Need to find some way to make this get reused for editable waveforms, though.

var asp: AudioStreamPlayer = null
var rc: bool = false

func mcir_apply_pattern_aftertouch():
	if asp != null:
		asp.volume_db = linear2db(stored.vol_mul)
		asp.pitch_scale = note2pitch(stored.note)

func mcir_exec_pattern_setup(data: MCIRDataPatternAudio, env: MCIRExecEnvironmentalData):
	asp = AudioStreamPlayer.new()
	add_child(asp)
	rc = data.release_continue
	asp.stream = data.stream
	asp.bus = env.audio_bus
	mcir_apply_pattern_aftertouch()
	asp.play(env.start_pos)

func mcir_exec_pattern_release():
	if not rc:
		asp.stop()

func mcir_exec_pattern_time() -> float:
	return asp.get_playback_position()

func _process(_delta):
	if not asp.playing:
		queue_free()
