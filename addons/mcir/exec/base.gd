class_name MCIRExecPatternBase
extends MCIRExecAdvancable

# Set by exec_patterns
var base_ignore_note: bool = false
var stored: MCIRExecAftertouchState = MCIRExecAftertouchState.new()

# this can be run at any time, including pre-setup
# it'll be used by envelopes and such
func mcir_exec_pattern_aftertouch(at: MCIRExecAftertouchState):
	stored.copy(at)
	if base_ignore_note:
		stored.note = 0
	mcir_apply_pattern_aftertouch()

func mcir_apply_pattern_aftertouch():
	pass

# can't use type here for various reasons
func mcir_exec_pattern_setup(data, env: MCIRExecEnvironmentalData):
	print("Pattern executive for " + str(data.mcir_pattern_id()) + " did no setup!")
	queue_free()

# releases the note
func mcir_exec_pattern_release():
	pass

# float for visualizing in editor
func mcir_exec_pattern_time() -> float:
	return 0.0

static func note2pitch(note: float) -> float:
	return pow(2, note / 12.0)
