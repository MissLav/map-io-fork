class_name MCIRExecPatternsDC
extends Node

# Decoupled for use by notes
# Do not use unless inside a pattern already

static func run(pattern: MCIRDataPatternBase, parent: Node, env: MCIRExecEnvironmentalData) -> MCIRExecPatternBase:
	return load("res://addons/mcir/exec_patterns.gd").run(pattern, parent, env)
