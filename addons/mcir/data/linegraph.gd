class_name MCIRDataLinegraph
extends Resource

export var lengths: PoolRealArray
export var values: PoolRealArray

func update(l: PoolRealArray, v: PoolRealArray):
	lengths = l
	values = v
	emit_changed()

func copy_from(other):
	update(other.lengths, other.values)

func sample(p: float, def: float) -> float:
	if len(lengths) == 0:
		return def
	var idx = 0
	var vlen = 0.0
	# iterate forward, lowering position as necessary
	for l in lengths:
		if p < l:
			vlen = l
			break
		else:
			p -= l
		idx += 1
	if idx >= len(lengths) - 1:
		# no interpolation possible: no v2
		return values[len(values) - 1]
	if (p <= 0) or (vlen == 0.00):
		# no interpolation possible: position <= start, empty len
		return values[idx]
	#print(vlen)
	return lerp(values[idx], values[idx + 1], p / vlen)
