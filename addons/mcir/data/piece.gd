class_name MCIRDataPiece
extends MCIRDataPieceBase

export var editor_exit: int = 0
# List of connected pieces.
export var connections: PoolStringArray
# List of enabled patterns.
export var patterns: PoolStringArray

export var bpm = 140.0
export var beats = 16.0

func beat_time_usec(beat: float) -> int:
	return int(1000000.0 * MCIRMaths.beat_time(bpm, beat))

func get_connections():
	return connections

func add_connection(a: String):
	connections.append(a)

func del_connection(a: String):
	# get rid of first instance of connection
	var idx = 0
	for v in connections:
		if v == a:
			connections.remove(idx)
			return
		idx += 1

func rename_connections(a: String, b: String):
	for i in range(len(connections)):
		if connections[i] == a:
			connections[i] = b

func rename_pattern_refs(a: String, b: String) -> int:
	var total: int = 0
	for i in range(len(patterns)):
		if patterns[i] == a:
			patterns[i] = b
			total += 1
	return total
