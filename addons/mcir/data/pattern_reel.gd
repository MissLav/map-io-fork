class_name MCIRDataPatternReel
extends MCIRDataPatternBase

# Timing
export var bpm: float = 140
# Editor extent
export var editor_beats: float = 16

export var sustain_start: float = -1
export var sustain_end: float = -1

# Must be sorted
# fun fact: I forgot that putting = [] here causes glitches
export var notes: Array

func _init():
	notes = []

func mcir_pattern_id() -> String:
	return "reel"

func add_note(n: MCIRDataPatternReelNote):
	editor_beats = max(editor_beats, n.start + n.length)
	notes.append(n)
	notes.sort_custom(MCIRDataPatternReelNote, "compare")
	emit_changed()

func rm_note(n: MCIRDataPatternReelNote):
	notes.erase(n)
	emit_changed()

func poke_note(n: MCIRDataPatternReelNote):
	editor_beats = max(editor_beats, n.start + n.length)
	notes.sort_custom(MCIRDataPatternReelNote, "compare")
	emit_changed()

func rename_pattern_refs(from: String, to: String) -> int:
	var total: int = 0
	for n in notes:
		var note: MCIRDataPatternReelNote = n
		if note.pattern == from:
			note.pattern = to
			total += 1
	return total

func dup_pattern():
	var b = duplicate()
	b.notes = []
	for v in notes:
		var x = v.dup_note()
		b.notes.append(x)
	return b
