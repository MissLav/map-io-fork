class_name MCIRDataCircuit
extends Resource

# Named MCIRDataPiece instances
export var pieces: Dictionary
# MCIRDataPatternBase library
export var patterns: Dictionary

func _init():
	pieces = {}
	patterns = {}

func any_piece_connected_to(piece: String):
	for v in pieces.values():
		var piece_base: MCIRDataPieceBase = v
		if piece_base.has_connection(piece):
			return true
	return false

# It is assumed that the caller does the actual renaming of the pattern.
# Note that from == to is legal and just counts references.
func rename_pattern_refs(from: String, to: String) -> int:
	var total = 0
	for v in pieces.values():
		total += v.rename_pattern_refs(from, to)
	for v in patterns.values():
		total += v.rename_pattern_refs(from, to)
	return total
