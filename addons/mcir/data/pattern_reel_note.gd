class_name MCIRDataPatternReelNote
extends Resource

export var pattern: String = ""
export var start: float = 0.0
export var length: float = 1.0
export var note: float = 0.0

export var lg_volume: Resource
export var lg_pitch: Resource
export var lg_pan: Resource

func _init():
	 lg_volume = MCIRDataLinegraph.new()
	 lg_pitch = MCIRDataLinegraph.new()
	 lg_pan = MCIRDataLinegraph.new()

static func compare(a, b):
	if a.start < b.start:
		return true
	return false

func dup_note():
	var n = duplicate()
	n.lg_volume.copy_from(lg_volume)
	n.lg_pitch.copy_from(lg_pitch)
	n.lg_pan.copy_from(lg_pan)
	return n
