extends Control

export var camera_body_path: NodePath
onready var player_body: KinematicBody = get_node(camera_body_path)
var camera: Camera

var mouse_captured = false

var cam_rotation: Vector2 = Vector2(deg2rad(-90), 0)
var velocity: Vector3 = Vector3()

func _ready():
	camera = player_body.get_node("Camera")

func _physics_process(dt):
	var speed = 1024
	var gravity = 1024
	var basis: Basis = Basis(Vector3(cam_rotation.y, cam_rotation.x, 0))
	var move_basis: Basis = Basis(Vector3(0, cam_rotation.x, 0))
	camera.transform = Transform(basis, camera.transform.origin)
	var movement: Vector3 = Vector3()
	if Input.is_key_pressed(KEY_W):
		movement += Vector3(0, 0, -speed)
	if Input.is_key_pressed(KEY_S):
		movement += Vector3(0, 0, speed)
	if Input.is_key_pressed(KEY_A):
		movement += Vector3(-speed, 0, 0)
	if Input.is_key_pressed(KEY_D):
		movement += Vector3(speed, 0, 0)
	velocity += move_basis.xform(movement) * dt * Vector3(1, 0.25, 1)
	velocity += Vector3(0, gravity * -dt, 0)
	# we want infinite inertia because otherwise RigidBodies can cause us to ignore StaticBodies
	# this is *obviously terrible*
	velocity = player_body.move_and_slide(velocity, Vector3(0, 1, 0), false, 4, 0.78, true)
	if player_body.is_on_floor():
		if Input.is_key_pressed(KEY_SPACE):
			velocity *= Vector3(1.5, 1, 1.5) # nyehheheh
			velocity += Vector3(0, 512, 0)
		velocity *= Vector3(0.95, 1, 0.95) # ew not dt-based

func _gui_input(event):
	if event is InputEventMouseButton:
		if (event.button_index == 1) and event.pressed:
			if mouse_captured:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
				mouse_captured = false
				accept_event()
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				mouse_captured = true
				accept_event()
		elif (event.button_index == 2) and event.pressed:
			var inst: RigidBody = preload("./throw_me.tscn").instance()
			var gt = camera.global_transform
			inst.translation = gt.origin + gt.basis.xform(Vector3(0, 0, -32))
			inst.linear_velocity = gt.basis.xform(Vector3(0, 0, -512))
			player_body.get_parent().add_child(inst)
			accept_event()
	elif event is InputEventMouseMotion:
		if mouse_captured:
			var movement = event.relative
			cam_rotation -= movement / 512
			if cam_rotation.y < -1.57:
				cam_rotation.y = -1.57
			if cam_rotation.y > 1.57:
				cam_rotation.y = 1.57
