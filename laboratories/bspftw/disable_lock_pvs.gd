extends VBoxContainer

onready var vis = $"../../../Vis"
onready var dPVS = $DisablePVS
onready var lPVS = $LockPVS
onready var mon = $Monitor

func _process(_delta):
	mon.text = str(len(vis.mesh_pvs))
	vis.r_lockpvs = lPVS.pressed
	vis.r_novis = dPVS.pressed
