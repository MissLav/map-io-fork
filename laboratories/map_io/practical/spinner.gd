extends CSGBox

func _process(delta):
	rotate_x(delta * 1.9)
	rotate_y(delta * 1.2)
	rotate_z(delta * 1.5)

