extends Spatial

export var xmap: Resource
onready var map_real: MapIODataMap = xmap

func _ready():
	run_experiment("cube")

func cube():
	var root = MapIOMathsConvex.new()
	root.init_cube(64)
	return [root]

func hex1():
	var root = MapIOMathsConvex.new()
	root.init_cube(64)
	root.cut_plane(Plane(Vector3(0.5, 0.5, 0.5).normalized(), 0), null)
	return [root]

func half():
	var root = MapIOMathsConvex.new()
	root.init_cube(64)
	root.cut_plane(Plane(Vector3(0.0, 1.0, 0.0), 0), null)
	return [root]

func halfhex():
	var root = MapIOMathsConvex.new()
	root.init_cube(64)
	root.cut_plane(Plane(Vector3(0.5, 0.5, 0.5).normalized(), 0), null)
	root.cut_plane(Plane(Vector3(0.0, 1.0, 0.0), 0), null)
	return [root]

func crush():
	var root = MapIOMathsConvex.new()
	root.init_cube(64)
	root.cut_plane(Plane(Vector3(0.0, 1.0, 0.0), 0), null)
	root.cut_plane(Plane(Vector3(0.0, -1.0, 0.0), 0), null)
	return [root]

func across():
	var root = MapIOMathsConvex.new()
	root.init_cube(64)
	var root2 = MapIOMathsConvex.new()
	root.cut_plane(Plane(Vector3(1, 0.5, 0).normalized(), 0), null, root2, null)
	return [root, root2]

func map(n = -1):
	var convexes = []
	var worldspawn: MapIODataEntity = map_real.entities[0]
	for v in worldspawn.brushes:
		var root = MapIOMathsConvex.new()
		root.init_cube(256)
		for i in range(len(v.normals)):
			if n == i:
				break
			root.cut_plane(v.get_plane(i), null)
		convexes.push_back(root)
	return convexes

func map1():
	return map(1)

func map2():
	return map(2)

func map3():
	return map(3)

func map4():
	return map(4)

func map5():
	return map(5)

func run_experiment(name: String):
	for v in get_children():
		v.queue_free()
	var convexes = call(name)
	for c in convexes:
		var cv: MapIOMathsConvex = c
		var mi = MeshInstance.new()
		mi.mesh = MapIOMathsAux.convex_to_debug_mesh(cv)
		mi.material_override = preload("white.tres")
		add_child(mi)
