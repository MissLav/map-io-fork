extends Button

func _pressed():
	var ev = get_tree().current_scene.find_node("MapIOExperimentViewer")
	ev.run_experiment(text)
