extends LineEdit

export var synth_path: NodePath
onready var synth = get_node(synth_path)

func _ready():
	connect("text_changed", self, "_text_changed_really")

func _text_changed_really(text: String):
	synth.midi_event(PoolByteArray([0xC0, int(text)]))
