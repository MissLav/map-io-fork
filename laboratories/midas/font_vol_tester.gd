# This is a tool used to tune the included soundfont.
# At time of writing, the balancing on this thing is *awful*...

extends Button

export var font_path: NodePath
onready var font: Node = get_node(font_path)

onready var status: Label = $"../Label"

# each note is [program, note, remainingTimeOn, remainingTimeOff, name]
onready var notes: Array = []
var current_note
var peak: float = -INF

signal midi_event(ev)

func _pressed():
	status.text = ""
	notes = []
	var filenames = {}
	for nt in font.get_children():
		if nt.name == "Percussion":
			# note: percussion should be super-overvolumed 
			# while everything else is standardized at -27.0
			# (ideally, but anything in the -27 range is fine accounting for error)
			# percussion should be MUCH HIGHER
			# say, -17
			# do special handling
			for sp in nt.get_children():
				var spc: MidasSynthLayer = sp
				if len(spc.specific_notes) != 0:
					notes.push_back([255, spc.specific_notes[0], 1.5, 1.5, "P" + spc.name])
		else:
			var name = nt.filename
			if nt.filename == "":
				name = nt.name
			if not (name in filenames):
				filenames[name] = true
				notes.push_back([int(nt.name.split("-")[0]), 0x40, 1.5, 1.5, name])

func _process(delta):
	peak = max(peak, AudioServer.get_bus_peak_volume_left_db(0, 0))
	if current_note == null:
		if len(notes) > 0:
			current_note = notes.pop_front()
			emit_signal("midi_event", PoolByteArray([0xC0, current_note[0]]))
			emit_signal("midi_event", PoolByteArray([0x90, current_note[1], 0x40]))
			status.text += current_note[4]
			peak = -INF
		return
	if current_note[2] > 0:
		current_note[2] -= delta
		if current_note[2] <= 0:
			emit_signal("midi_event", PoolByteArray([0x90, current_note[1], 0x00]))
		return
	if current_note[3] > 0:
		current_note[3] -= delta
		if current_note[3] <= 0:
			status.text += " " + str(peak) + "\n"
			current_note = null
		return
