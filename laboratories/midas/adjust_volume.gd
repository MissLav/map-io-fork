extends Range

func _ready():
	value = round(AudioServer.get_bus_volume_db(1))
	connect("value_changed", self, "_changed")

func _changed(val):
	AudioServer.set_bus_volume_db(1, val)

func _process(_delta):
	var x = AudioServer.get_bus_peak_volume_left_db(1, 0)
	var y = AudioServer.get_bus_peak_volume_right_db(1, 0)
	$"../ProgressBar".value = max(x, y)
