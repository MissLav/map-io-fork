extends LineEdit

export var synth_path: NodePath
onready var synth = get_node(synth_path)

var base: int = 60

var notes: Dictionary = {}

var keymap: Dictionary = {
	KEY_BACKSLASH: -1,
	KEY_Z: 0,
	KEY_S: 1,
	KEY_X: 2,
	KEY_D: 3,
	KEY_C: 4,
	
	KEY_V: 5,
	KEY_G: 6,
	KEY_B: 7,
	KEY_H: 8,
	KEY_N: 9,
	KEY_J: 10,
	KEY_M: 11,
	KEY_COMMA: 12,
	KEY_L: 13,
	KEY_PERIOD: 14,
	KEY_SEMICOLON: 15,
	KEY_SLASH: 16
}

func _ready():
	var enghex = Engine.get_version_info().hex
	if enghex >= 0x030204 and enghex <= 0x030302:
		print("MIDI input has been disabled, see: https://github.com/godotengine/godot/issues/46183")
	else:
		OS.open_midi_inputs()
	connect("text_changed", self, "_text_changed_really")

func _text_changed_really(text: String):
	base = int(text)

func map_to_key(ev : InputEventKey) -> int:
	if keymap.has(ev.scancode):
		return keymap[ev.scancode]
	return 0xFFFF

func _input(event):
	if event is InputEventKey:
		if event.echo:
			return
		var k = map_to_key(event)
		if k != 0xFFFF:
			k += base
			if notes.has(k):
				notes[k].off()
				notes.erase(k)
			if event.pressed:
				synth.midi_event(PoolByteArray([0x90, k, 64]))
			else:
				synth.midi_event(PoolByteArray([0x80, k, 127]))
	if event is InputEventMIDI:
		if event.message == MIDI_MESSAGE_NOTE_ON:
			synth.midi_event(PoolByteArray([0x90 + event.channel, event.pitch, event.velocity]))
		elif event.message == MIDI_MESSAGE_NOTE_OFF:
			synth.midi_event(PoolByteArray([0x80 + event.channel, event.pitch, event.velocity]))
		print("Input " + str(event.message) + " " + str(event.pitch) + " " + str(event.velocity))
