extends Label

export var synth_path: NodePath
onready var synth = get_node(synth_path)

func _process(_delta):
	var tx = ""
	for k in range(16):
		var pn = 0
		if synth.programs_raw.has(k):
			pn = synth.programs_raw[k]
		var stat = synth._get_channel_program(k)
		if k != 0:
			tx += "\n"
		tx += str(k) + ":"
		if stat != null:
			tx += " P[" + str(pn) + "->" + stat.name + "]"
		var sound_count = 0
		if synth.sounds.has(k):
			sound_count = len(synth.sounds[k])
		tx += " N[" + str(sound_count) + "]"
	text = tx
