extends MenuButton

export(Array, PackedScene) var scenes = []

func _ready():
	get_popup().connect("index_pressed", self, "_selected_thing")

func _selected_thing(idx):
	get_tree().change_scene_to(scenes[idx])
