extends CanvasLayer

var last_scene = null

func _process(_delta):
	var cs = get_tree().current_scene
	if cs != last_scene:
		last_scene = cs
		if cs == null:
			return
		var panel = preload("selector_core.tscn").instance()
		var sa = cs.find_node("LabSelectorAttachmentPoint")
		if sa != null:
			sa.get_parent().add_child_below_node(sa, panel)
		else:
			add_child(panel)
