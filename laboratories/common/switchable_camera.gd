extends KinematicBody

onready var camera: Camera = $Camera

var fps_mode = false
var velocity = Vector3.ZERO
var mouse_motion = Vector2.ZERO
export var spd = 32
var tmr = 0.0
var was_tab_down = false

func _physics_process(delta):
	var tab_down = Input.is_key_pressed(KEY_TAB)
	if tab_down and not was_tab_down:
		fps_mode = not fps_mode
		if fps_mode:
			velocity = Vector3.ZERO
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if fps_mode:
		_process_fps(delta)
	else:
		_process_fly(delta)
	# clear per-process
	was_tab_down = tab_down
	mouse_motion = Vector2.ZERO

func _exit_tree():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _get_intent() -> Vector3:
	var intent = Vector3.ZERO
	if Input.is_key_pressed(KEY_W):
		intent += Vector3.FORWARD
	if Input.is_key_pressed(KEY_S):
		intent += Vector3.BACK
	if Input.is_key_pressed(KEY_A):
		intent += Vector3.LEFT
	if Input.is_key_pressed(KEY_D):
		intent += Vector3.RIGHT
	return intent

func _get_dir(d: Vector3, flat: bool) -> Vector3:
	var basis = camera.transform.basis
	if flat:
		# this is implemented in an interestingly dodgy way to allow for strafe-running
		# and to account for any camera rotation
		basis.x = (basis.x * Vector3(1, 0, 1)).normalized()
		basis.z = (basis.z * Vector3(1, 0, 1)).normalized()
	return basis.xform(_get_intent())

func _process_fps(delta):
	var intent = _get_intent()
	var intent_dir = _get_dir(intent, true)
	velocity += Vector3.DOWN * 10 * delta
	velocity = move_and_slide(velocity, Vector3.UP, true, 4, 0.785398, false)
	for i in range(get_slide_count()):
		var kc = get_slide_collision(i)
		if kc.normal.y < 0.5:
			if kc.collider is RigidBody:
				var rb: RigidBody = kc.collider
				rb.apply_impulse(kc.position - rb.global_transform.origin, intent_dir * delta * 250)
	var accel = 0.0
	if is_on_floor():
		accel = 64.0
		velocity *= 0.9
		if Input.is_key_pressed(KEY_SPACE):
			velocity += Vector3.UP * 7
	else:
		accel = 8.0
	velocity += intent_dir * delta * accel
	camera.rotate_object_local(Vector3.RIGHT, mouse_motion.y / -200.0)
	camera.rotate_y(mouse_motion.x / -200.0)
	mouse_motion = Vector2.ZERO

func _process_fly(delta):
	if Input.is_key_pressed(KEY_I):
		camera.rotate_object_local(Vector3.RIGHT, delta * 2)
	if Input.is_key_pressed(KEY_K):
		camera.rotate_object_local(Vector3.RIGHT, delta * -2)
	if Input.is_key_pressed(KEY_J):
		camera.rotate_y(delta * 2)
	if Input.is_key_pressed(KEY_L):
		camera.rotate_y(delta * -2)
	var intent = _get_intent()
	translate(_get_dir(intent, false) * delta * spd)
	tmr += delta
	if tmr >= 0.01:
		if Input.is_key_pressed(KEY_Q):
			spd *= 1.02
			spd = min(spd, 0x10000)
		if Input.is_key_pressed(KEY_E):
			spd *= 0.98
			spd = max(spd, 1)
		tmr = 0.0

func _input(i):
	if i is InputEventMouseMotion:
		mouse_motion += i.relative
