extends Label

func _process(delta):
	var txt = ""
	txt += str(VisualServer.get_render_info(VisualServer.INFO_MATERIAL_CHANGES_IN_FRAME))
	txt += " material changes\n"
	txt += str(VisualServer.get_render_info(VisualServer.INFO_DRAW_CALLS_IN_FRAME))
	txt += " draw calls\n"
	txt += str(VisualServer.get_render_info(VisualServer.INFO_VERTICES_IN_FRAME))
	txt += " vertices\n"
	txt += str(VisualServer.get_render_info(VisualServer.INFO_OBJECTS_IN_FRAME))
	txt += " objects\n"
	text = txt
